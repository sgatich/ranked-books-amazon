"""Add a relationship for the Books table

Revision ID: 450695a2839c
Revises: 97d2433d0c14
Create Date: 2018-01-31 22:39:50.874084

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, Integer, ForeignKey


# revision identifiers, used by Alembic.
revision = '450695a2839c'
down_revision = '97d2433d0c14'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "statistics",
        Column("rank", Integer)
    )


def downgrade():
    pass
