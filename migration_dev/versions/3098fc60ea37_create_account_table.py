"""Create account table

Revision ID: 3098fc60ea37
Revises: 
Create Date: 2018-01-31 04:37:51.859254

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, DateTime


# revision identifiers, used by Alembic.
revision = '3098fc60ea37'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("prices",
                  Column("timestamp", DateTime()))


def downgrade():
    pass
