"""Add a table for the statistics

Revision ID: 97d2433d0c14
Revises: 3098fc60ea37
Create Date: 2018-01-31 19:24:22.419087

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, DateTime, Integer, Float, ForeignKey


# revision identifiers, used by Alembic.
revision = '97d2433d0c14'
down_revision = '3098fc60ea37'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table("statistics",
                    Column("id", Integer, primary_key=True),
                    Column("average", Float),
                    Column("minimum", Float),
                    Column("maximum", Float),
                    Column("timestamp", DateTime),
                    Column("book_id", Integer, ForeignKey("books.id"))
                    )


def downgrade():
    pass
