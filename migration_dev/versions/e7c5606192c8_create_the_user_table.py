"""Create the User table

Revision ID: e7c5606192c8
Revises: 450695a2839c
Create Date: 2018-02-09 18:32:18.594938

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String


# revision identifiers, used by Alembic.
revision = 'e7c5606192c8'
down_revision = '450695a2839c'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table("users",
                    Column("id", Integer, primary_key=True),
                    Column("username", String(length=150)),
                    Column("email", String(length=150)),
                    Column("password_hash", String(length=1000))
                    )


def downgrade():
    pass
