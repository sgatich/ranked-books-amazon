from bs4 import BeautifulSoup
import requests
import bottlenose
import random
from models import *
from grab import Grab
from datetime import date
import time
import threading


MWS_ACCESS_KEY = 'AKIAJHCGHX5QYSBUNKRQ'
MWS_SECRET_KEY = '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'
ACCOUNT_ID = 'AWM2BZYAVU72X'
MARKETPLACE_ID = 'ATVPDKIKX0DER'


base_link = "https://www.amazon.com/best-sellers-books-Amazon/zgbs/books/ref=zg_bs_pg_3?_encoding=UTF8&pg="
base_link_first_page = base_link + str(1)

base_amazon_link = "https://www.amazon.com"
base_categories_xpath = "//ul[@id='zg_browseRoot']/ul"

like_new_suffix = "ref=olp_f_usedVeryGood?ie=UTF8&f_usedLikeNew=true"
very_good_suffix = "ref=olp_f_usedLikeNew?ie=UTF8&f_usedVeryGood=true"
good_suffix = "ref=olp_f_usedAcceptable?ie=UTF8&f_usedGood=true"


class Scraper:
    def __init__(self):
        self.outputFile = open("most-selling.txt", "w")
        self.outputFile.close()
        self.session = Session()
        self.g = Grab()
        self.thread_id = threading.get_ident()

        proxies_entries = self.session.query(Proxy).all()
        self.proxies_list = []

        for proxy_entry in proxies_entries:
            self.proxies_list.append(proxy_entry.proxy)

        metadata.create_all(bind=engine)

        # self.switch_proxy()
        # self.g.go("https://google.com")

    def __del__(self):
        self.session.close()
        del self.g

    def switch_proxy(self):
        self.current_proxy = random.choice(self.proxies_list)
        print("Switching to proxy: " + self.current_proxy)
        self.g.setup(proxy=self.current_proxy, proxy_type="http", connect_timeout=10, timeout=10)

    def get_books_obtained(self):
        return self.books_obtained

    def run(self, start_quantity, end_quantity):
        step = 5
        self.books_obtained = start_quantity

        while self.books_obtained <= end_quantity:
            latest_book_link = self.session.query(BookLink).order_by(BookLink.date)[0]
            latest_date = latest_book_link.date

            most_selling_dict = {}
            start_number = self.books_obtained
            end_number = self.books_obtained + step

            for book_link_entry in self.session.query(BookLink).\
                    filter(BookLink.date == latest_date)[start_number:end_number]:
                most_selling_dict[book_link_entry.link] = book_link_entry.category

            for book in most_selling_dict.keys():
                asin = ""
                splitted_link = book.split("dp/")

                if len(splitted_link) == 2:
                    without_dp = splitted_link[1]
                    asin = without_dp.split("/")[0]

                # p = MWSProductsInterface(MWS_ACCESS_KEY, MWS_SECRET_KEY, ACCOUNT_ID)
                # res = p.get_lowest_offer_listings_for_asin(MARKETPLACE_ID, ["069289344X"], condition="New")

                print(asin)
                # print(res)
                # self.switch_proxy()
                # self.obtain_book_info(book, most_selling_dict[book])

            self.books_obtained += step
            time.sleep(1)

        # print("Scraper.run. Task finished. ")
        Session.remove()

    def write_proxies_to_db(self):
        self.g = Grab()
        proxies_file = open("proxies-list", "r")
        proxies_list = proxies_file.readlines()

        for proxy in proxies_list:
            proxy = proxy.replace("\n", "")

            try:
                self.g.setup(proxy=proxy, proxy_type="http", connect_timeout=10, timeout=10)
                self.g.go("https://www.yandex.ru/")
                print("Testing proxy: " + proxy)
            except:
                print("Fail.")
                continue
            print("Success.")
            proxy_entry = Proxy(proxy=proxy)
            self.session.add(proxy_entry)
            self.commit_session()

    #Depth-first search
    def obtain_categories_main(self):
        self.g.setup(proxy="35.186.187.230:3128", proxy_type="http", connect_timeout=10, timeout=10)
        try:
            self.g.go(base_link_first_page)
        except:
            self.switch_proxy()
            self.g.go(base_link_first_page)

        categories_nodes = self.g.doc.select(base_categories_xpath)
        for category_node in categories_nodes:
            self.obtain_categories_recursive(base_categories_xpath, "Books", base_link_first_page)

    def obtain_categories_recursive(self, current_xpath, category_path, link):
        print(link)
        ul_nodes = self.g.doc.select(current_xpath + "/ul")

        category_paths_list = category_path.split("->")

        previous_category = "Books"
        if len(category_paths_list) > 2:
            previous_category = category_paths_list[-2]

        if len(ul_nodes) == 0:
        #We are visiting a leaf node
        #We must do: read a selected category, add it to the database
            li_selected_nodes = self.g.doc.select(current_xpath + "/li/span[@class='zg_selected']")
            if len(li_selected_nodes) > 0:
                category_name = li_selected_nodes[0].text()
                name = previous_category + "->" + category_name
                print(name)

                try:
                    category_entry = Category(name=name, link=link)
                    self.session.add(category_entry)
                    self.commit_session()
                except:
                    self.session.rollback()
                    pass
            return

        current_xpath = current_xpath + "/ul"

        #Else: find unordered list of links, read them and text they contain, visit links
        categories_links_nodes = self.g.doc.select(current_xpath + "/li/a")
        for category_link_node in categories_links_nodes:
            category_text = category_link_node.text()
            category_path = category_path + "->" + category_text

            category_link_address = category_link_node.attr("href")
            try:
                self.g.go(category_link_address)
            except:
                self.switch_proxy()
                try:
                    self.g.go(category_link_address)
                except:
                    pass

            self.obtain_categories_recursive(current_xpath, category_path, category_link_address)
            category_path_suffix = "->" + category_text
            category_path = category_path.replace(category_path_suffix, "")

    def obtain_most_selling(self, start_category, end_category):
        print("Thread#" + str(self.thread_id) + " - Obtaining the most selling.")
        categories_list = self.session.query(Category).order_by(Category.name)[start_category:end_category]

        i = start_category
        for category in categories_list:
            self.switch_proxy()
            print("Thread#" + str(self.thread_id) + " - Category#" + str(i))
            self.obtain_most_selling_сategory(category.link, category)
            i += 1

        print("Thread#" + str(self.thread_id) + " - Links.run. Task finished. ")
        Session.remove()

    def obtain_most_selling_сategory(self, link_category, category_entry):
        link_category = link_category.split("ref")[0]
        link_category = link_category + "ref=zg_bs_pg_2?_encoding=UTF8&pg="

        most_selling_list = []
        for i in range(1, 6):
            current_link = link_category + str(i)
            print(current_link)
            try:
                self.g.go(current_link)
            except:
                self.switch_proxy()
                try:
                    self.g.go(current_link)
                except:
                    pass

            link_nodes = self.g.doc.select("//div[@id='zg_centerListWrapper']//a[@class='a-link-normal']")
            print(len(link_nodes))

            for link_node in link_nodes:
                link_node_divs = link_node.select(".//div")
                if len(link_node_divs) > 0:
                    print(link_node.attr("href"))
                    most_selling_list.append(link_node.attr("href"))

        for most_selling in most_selling_list:
            book_link_entry = BookLink(link=base_amazon_link + most_selling,
                                       date=date.today())
            book_link_entry.category = category_entry
            self.session.add(book_link_entry)
            try:
                self.commit_session()
            except:
                print("Thread#" + str(self.thread_id) +" - The database already has this link.")
                self.session.rollback()
                pass


    def obtain_book_info_by_asin(self, asin):
        pass

    def obtain_book_info(self, book_link, category_entry):
        print("Thread#" + str(self.thread_id) + " - Processing book: " + book_link)
        try:
            self.g.go(book_link)
        except:
            self.switch_proxy()
            self.g.go(book_link)

        details_table_nodes = self.g.doc.select("//table[@id='productDetailsTable']")

        cover_type = ""

        if len(details_table_nodes) > 0:
            details = details_table_nodes[0].text()
            if "Hardcover" not in details and "Paperback" not in details:
                return
            else:
                if "Hardcover" in details:
                    cover_type = "Hardcover"
                elif "Paperback" in details:
                    cover_type = "Paperback"
                else:
                    return
        else:
            return

        book_title = ""

        book_title_nodes = self.g.doc.select("//span[@id='productTitle']")

        if len(book_title_nodes) > 0:
            book_title = book_title_nodes[0].text()
            print("Book title: " + book_title)

        newer_available = False
        newer_link = ""
        newer_available_nodes = \
            self.g.doc.select("//div[@id='newer-version']//a[@class='a-size-base a-link-normal']")

        if len(newer_available_nodes) > 0:
            newer_available = True
            newer_link = base_amazon_link + newer_available_nodes[0].text()

        book_isbn = ""
        self.prices_entries_list = []

        book_details_nodes = self.g.doc.select("//table[@id='productDetailsTable']//li")
        twister_row_nodes = self.g.doc.select("//div[@id='twister']//"
                                              "table[@class='a-normal a-spacing-none']//tr")

        old_price_nodes = self.g.doc.select("//div[@id='mediaOlp']//span[@class='olp-padding-right']//a")

        if len(twister_row_nodes) == 0:
            print("ERROR! Obtaining info again. ")
            self.update_or_create_compromised(self.current_proxy)
            self.switch_proxy()
            print("Processing book again: " + book_link)
            try:
                self.g.go(book_link)
            except:
                self.switch_proxy()
                self.g.go(book_link)

            book_title = ""

            book_title_nodes = self.g.doc.select("//span[@id='productTitle']")

            if len(book_title_nodes) > 0:
                book_title = book_title_nodes[0].text()
                print("Book title: " + book_title)

            newer_available = False
            newer_link = ""
            newer_available_nodes = \
                self.g.doc.select("//div[@id='newer-version']//a[@class='a-size-base a-link-normal']")

            if len(newer_available_nodes) > 0:
                newer_available = True
                newer_link = base_amazon_link + newer_available_nodes[0].text()

            book_isbn = ""
            self.prices_entries_list = []

            book_details_nodes = self.g.doc.select("//table[@id='productDetailsTable']//li")
            twister_row_nodes = self.g.doc.select("//div[@id='twister']//"
                                                  "table[@class='a-normal a-spacing-none']//tr")
            old_price_nodes = self.g.doc.select("//div[@id='mediaOlp']//span[@class='olp-padding-right']//a")

        if len(book_title) == 0:
            return

        for node in twister_row_nodes:
            if cover_type in node.text():
                self.process_hardcover_or_paperback(node, cover_type)

        if len(self.prices_entries_list) == 0:
            if len(old_price_nodes) > 0:
                print("Obtaining prices (old type of link).")
                self.process_old_hardcover_or_paperback(old_price_nodes, cover_type)

        if len(self.prices_entries_list) == 0:
            return

        rank = ""

        for node in book_details_nodes:
            if "ISBN-10" not in node.text():
                continue
            else:
                book_isbn = node.text().replace("ISBN-10: ", "")

        for node in book_details_nodes:
            if "Rank" not in node.text():
                continue
            else:
                rank_node_text = node.text()
                rank_node_text = rank_node_text.split("#")[1]
                rank_node_text = rank_node_text.split(" in Books (")[0]
                rank = rank_node_text.replace(",", "")

        print("ISBN: " + book_isbn)
        print("Rank: " + rank)

        try:
            if int(rank) > 300000 or rank == "":
                return
        except:
            return

        print("Calculating profit for a book: " + book_link)

        profit_like_new = self.calculate_profit_like_new(cover_type)
        profit_very_good = self.calculate_profit_very_good(cover_type)
        profit_ratio = self.calculate_profit_ratio(profit_like_new, profit_very_good)

        book_entry = Book(title=book_title,
                          isbn=book_isbn,
                          newer_edition_link=newer_link,
                          newer_edition_available=newer_available,
                          link=book_link,
                          rank=int(rank),
                          profit_like_new=profit_like_new,
                          profit_very_good=profit_very_good,
                          profit_ratio=profit_ratio,
                          cover_type=cover_type)

        book_entry.category = category_entry
        book_entry.prices = self.prices_entries_list

        #Here will be new code:

        self.session.add(book_entry)

        try:
            self.commit_session()
        except:
            print("The database already has this book.")
            self.session.rollback()
            return

    def process_old_hardcover_or_paperback(self, nodes, cover_type):
        for node in nodes:
            if "New" in node.text():
                new_link_text = base_amazon_link + node.attr("href")
                self.prices_entries_list += self.obtain_price_entries_for_book("New",
                                                                               cover_type,
                                                                               new_link_text)

            elif "Used" in node.text():
                used_nodes = []
                used_nodes.append(node)
                used_dict = self.obtain_links_for_used_categories(used_nodes)

                if "like_new" in used_dict:
                    self.prices_entries_list += self.obtain_price_entries_for_book("Like new",
                                                                                   cover_type,
                                                                                   used_dict["like_new"])
                if "very_good" in used_dict:
                    self.prices_entries_list += self.obtain_price_entries_for_book("Very good",
                                                                                   cover_type,
                                                                                   used_dict["very_good"])
                if "good" in used_dict:
                    self.prices_entries_list += self.obtain_price_entries_for_book("Good",
                                                                                   cover_type,
                                                                                   used_dict["good"])
            print(node.text())

    def process_hardcover_or_paperback(self, node, cover_type):
        new_links = node.select(".//td[@class='a-text-right dp-new-col']//a")
        for new_link in new_links:
            new_link_text = base_amazon_link + new_link.attr("href")
            print("A link to new: " + new_link_text)
            self.prices_entries_list += self.obtain_price_entries_for_book("New",
                                                                          cover_type,
                                                                          new_link_text)

        used_links = node.select(".//td[@class='a-text-right dp-used-col']//a")
        used_dict = self.obtain_links_for_used_categories(used_links)

        if "like_new" in used_dict:
            self.prices_entries_list += self.obtain_price_entries_for_book("Like new",
                                                                          cover_type,
                                                                          used_dict["like_new"])
        if "very_good" in used_dict:
            self.prices_entries_list += self.obtain_price_entries_for_book("Very good",
                                                                          cover_type,
                                                                          used_dict["very_good"])
        if "good" in used_dict:
            self.prices_entries_list += self.obtain_price_entries_for_book("Good",
                                                                          cover_type,
                                                                          used_dict["good"])
        print(node.text())

    def obtain_links_for_used_categories(self, used_links):
        used_dict = {}
        for used_link in used_links:
            used_link_text = base_amazon_link + used_link.attr("href")
            like_new_link = used_link_text.split("ref")[0] + like_new_suffix
            very_good_link = used_link_text.split("ref")[0] + very_good_suffix
            good_link = used_link_text.split("ref")[0] + good_suffix

            used_dict["like_new"] = like_new_link
            used_dict["very_good"] = very_good_link
            used_dict["good"] = good_link

            print(like_new_link)
            print(very_good_link)
            print(good_link)

        return used_dict

    def obtain_price_entries_for_book(self, condition, cover_type, link_to_sellers_area):
        prices_list = self.obtain_prices(link_to_sellers_area)
        shipping_prices_list = self.obtain_shipping_prices(link_to_sellers_area)
        sellers_names_list = self.obtain_sellers_names(link_to_sellers_area)
        sellers_ratings_list = self.obtain_sellers_ratings(link_to_sellers_area)

        new_draft_dict = {}
        new_draft_dict["prices"] = prices_list
        new_draft_dict["shipping_prices"] = shipping_prices_list
        new_draft_dict["sellers_names"] = sellers_names_list
        new_draft_dict["sellers_ratings"] = sellers_ratings_list
        new_draft_dict["condition"] = condition
        new_draft_dict["cover_type"] = cover_type

        return self.form_prices_entries_list(new_draft_dict)

    def obtain_prices(self, link):
        try:
            self.g.go(link)
        except:
            self.switch_proxy()
            self.g.go(link)
        price_elements = self.g.doc.select("//span[@class='a-size-large "
                                           "a-color-price olpOfferPrice a-text-bold']")
        prices = []

        for price_element in price_elements:
            price_text = price_element.text().replace("$", "")

            price_text = price_text.replace(",", "")
            prices.append(float(price_text))

        return self.shrink_to_five(prices)

    def obtain_shipping_prices(self, link):
        try:
            self.g.go(link)
        except:
            self.switch_proxy()
            self.g.go(link)
        shipping_info_elements = self.g.doc.select("//p[@class='olpShippingInfo']")
        shipping_prices = []

        for shipping_info_element in shipping_info_elements:
            if "free" in shipping_info_element.text().lower():
                shipping_prices.append(float(0))
            else:
                shipping_price_elements = shipping_info_element.select(".//span[@class='olpShippingPrice']")
                if len(shipping_price_elements) > 0:
                    shipping_price_text = shipping_price_elements[0].text()
                    shipping_price_text = shipping_price_text.replace("$", "")
                    shipping_price_text = shipping_price_text.replace(",", "")
                    shipping_prices.append(float(shipping_price_text))

        return self.shrink_to_five(shipping_prices)

    def obtain_sellers_names(self, link):
        try:
            self.g.go(link)
        except:
            self.switch_proxy()
            self.g.go(link)

        sellers_elements = self.g.doc.select("//div[@class='a-column a-span2 olpSellerColumn']")

        sellers_names = []

        for sellers_element in sellers_elements:
            image_elements = sellers_element.select(".//img")
            if len(image_elements) > 0:
                sellers_names.append("amazon.com")
            else:
                name_elements = sellers_element.select(".//a")
                if len(name_elements) > 0:
                    sellers_names.append(name_elements[0].text())

        return self.shrink_to_five(sellers_names)

    def obtain_sellers_ratings(self, link):
        try:
            self.g.go(link)
        except:
            self.switch_proxy()
            self.g.go(link)

        sellers_elements = self.g.doc.select("//div[@class='a-column a-span2 olpSellerColumn']")

        sellers_ratings = []

        for sellers_element in sellers_elements:
            p_elements = sellers_element.select(".//p[@class='a-spacing-small']")
            if len(p_elements) > 0:
                if "Just Launched" in p_elements[0].text():
                    sellers_ratings.append("Just Launched")
                else:
                    rating_elements = p_elements[0].select(".//a")
                    if len(rating_elements) > 0:
                        sellers_ratings.append(rating_elements[0].text())

            else:
                sellers_ratings.append("Amazon")

        return self.shrink_to_five(sellers_ratings)

    def form_prices_entries_list(self, info_dict):
        prices = info_dict["prices"]
        shipping_prices = info_dict["shipping_prices"]
        sellers_names = info_dict["sellers_names"]
        sellers_ratings = info_dict["sellers_ratings"]
        condition = info_dict["condition"]
        cover_type = info_dict["cover_type"]

        prices_entries = []

        if len(prices) != len(shipping_prices):
            print("ERROR! Invalid entries")
            return []
        if len(prices) != len(sellers_names):
            print("ERROR! Invalid entries")
            return []
        if len(prices) != len(sellers_ratings):
            print("ERROR! Invalid entries")
            return []

        for i in range(len(prices)):
            price_entry = Price(price=prices[i],
                                shipping=shipping_prices[i],
                                cover_type=cover_type,
                                condition=condition,
                                sellers_name=sellers_names[i],
                                sellers_rating=sellers_ratings[i])
            prices_entries.append(price_entry)

        return prices_entries


    def shrink_to_five(self, list_to_shrink):
        if len(list_to_shrink) > 5:
            list_to_shrink = list_to_shrink[:5]

        return list_to_shrink

    def commit_session(self):
        self.session.commit()

    def flush_session(self):
        self.session.flush()

    def update_or_create_compromised(self, proxy):
        compromised_entry = self.session.query(CompromisedProxy).\
            filter(CompromisedProxy.proxy == proxy).first()

        if compromised_entry:
            compromised_entry.times += 1
            self.commit_session()
        else:
            compromised_entry = CompromisedProxy(proxy=proxy, times=1)
            self.session.add(compromised_entry)
            self.commit_session()

    def update_or_create_book(self, book_entry):
        book_entry_found = self.session.query(Book). \
            filter(Book.link == book_entry.link).first()

        if book_entry_found:
            print("Thread#" + str(self.thread_id) + " - Updating book.")
            book_entry_found.prices = book_entry.prices
            book_entry_found.profit_like_new = book_entry.profit_like_new
            book_entry_found.profit_very_good = book_entry.profit_very_good
            book_entry_found.profit_ratio = book_entry.profit_ratio
        else:
            print("Thread#" + str(self.thread_id) + " - Creating book.")
            self.session.add(book_entry)

    def calculate_profit_like_new(self, cover_type):
        new_minimal_price = 100000
        new_minimal_shipping = 100000

        like_new_minimal_price = 100000
        like_new_minimal_shipping = 100000

        for price_entry in self.prices_entries_list:
            if price_entry.cover_type != cover_type:
                continue
            else:
                if price_entry.condition == "New":
                    if (price_entry.price + price_entry.shipping ) < (new_minimal_price + new_minimal_shipping):
                        new_minimal_price = price_entry.price
                        new_minimal_shipping = price_entry.shipping

                if price_entry.condition == "Like new":
                    if (price_entry.price + price_entry.shipping ) < (like_new_minimal_price + like_new_minimal_shipping):
                        like_new_minimal_price = price_entry.price
                        like_new_minimal_shipping = price_entry.shipping

        print("Calculating profit for like new. New minimal price: " + str(new_minimal_price))
        print("Calculating profit for like new. New minimal shipping: " + str(new_minimal_shipping))
        print("Calculating profit for like new. Like new minimal price: " + str(like_new_minimal_price))
        print("Calculating profit for like new. Like new minimal shipping: " + str(like_new_minimal_shipping))

        if new_minimal_price == 100000 or new_minimal_shipping == 100000 or like_new_minimal_price == 100000 or like_new_minimal_shipping == 100000:
            return 0
        else:
            profit = (new_minimal_price + new_minimal_shipping) * 0.84 - \
                     (like_new_minimal_price + like_new_minimal_shipping) - 1.35
            return profit

    def calculate_profit_very_good(self, cover_type):
        new_minimal_price = 100000
        new_minimal_shipping = 100000

        very_good_minimal_price = 100000
        very_good_minimal_shipping = 100000

        for price_entry in self.prices_entries_list:
            if price_entry.cover_type != cover_type:
                continue
            else:
                if price_entry.condition == "New":
                    if (price_entry.price + price_entry.shipping ) < (new_minimal_price + new_minimal_shipping):
                        new_minimal_price = price_entry.price
                        new_minimal_shipping = price_entry.shipping

                if price_entry.condition == "Very good":
                    if (price_entry.price + price_entry.shipping ) < (very_good_minimal_price + very_good_minimal_shipping):
                        very_good_minimal_price = price_entry.price
                        very_good_minimal_shipping = price_entry.shipping

        print("Calculating profit for very good. New minimal price: " + str(new_minimal_price))
        print("Calculating profit for very good. New minimal shipping: " + str(new_minimal_shipping))
        print("Calculating profit for very good. Very good minimal price: " + str(very_good_minimal_price))
        print("Calculating profit for very good. Very good minimal shipping: " + str(very_good_minimal_shipping))

        if new_minimal_price == 100000 or new_minimal_shipping == 100000 or very_good_minimal_price == 100000 or very_good_minimal_shipping == 100000:
            return 0
        else:
            profit = (new_minimal_price + new_minimal_shipping) * 0.84 - \
                     (very_good_minimal_price + very_good_minimal_shipping) - 1.35
            return profit

    def calculate_profit_ratio(self, like_new, very_good):
        minimal_profit = min(like_new, very_good)

        new_minimal_price = 100000
        new_minimal_shipping = 100000

        for price_entry in self.prices_entries_list:
            if price_entry.condition == "New":
                if (price_entry.price + price_entry.shipping) < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_entry.price
                    new_minimal_shipping = price_entry.shipping

        print("Calculating profit ratio. New minimal price: " + str(new_minimal_price))
        print("Calculating profit ratio. New minimal shipping: " + str(new_minimal_shipping))

        print("Calculating profit ratio. Minimal profit value: " + str(minimal_profit))

        if new_minimal_price == 100000 or new_minimal_shipping == 100000:
            return 0
        else:
            return minimal_profit / (new_minimal_price + new_minimal_shipping)