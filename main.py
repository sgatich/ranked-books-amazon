import sys
import time
import threading
from scraper import Scraper
from scraper_worker import ScraperWorker
from links_worker import LinksWorker
from models import Session, BookLink, Category


def main():
    if sys.argv[1] == "links":
        session = Session()

        number_categories = len(session.query(Category).all())

        workload = number_categories // 4

        first_worker = LinksWorker(0, workload - 1)
        second_worker = LinksWorker(workload, 2 * workload - 1)
        third_worker = LinksWorker(2 * workload, 3 * workload - 1)
        fourth_worker = LinksWorker(3 * workload, number_categories - 1)

        first_worker.start()
        second_worker.start()
        third_worker.start()
        fourth_worker.start()

    elif sys.argv[1] == "prices":
        stamp_one = time.time()

        first_worker = ScraperWorker(5000, 5499)
        second_worker = ScraperWorker(5500, 5999)
        third_worker = ScraperWorker(6000, 6499)
        fourth_worker = ScraperWorker(6500, 6999)

        first_worker.start()
        second_worker.start()
        third_worker.start()
        fourth_worker.start()

        stamp_two = time.time()
        print("Average time spent on a book: " + str((stamp_two - stamp_one)/500))

    elif sys.argv[1] == "proxies-db":
        amazonScraper = Scraper()
        amazonScraper.write_proxies_to_db()
    elif sys.argv[1] == "proxies-file":
        process_proxy_file()
    elif sys.argv[1] == "categories":
        amazonScraper = Scraper()
        amazonScraper.obtain_categories_main()
    else:
        print("Please provide a command line argument and relaunch the script.")
        print("Exit.")
        exit(0)

def process_proxy_file():
    proxies_file = open("proxies-list", "r")
    proxies_list = proxies_file.readlines()

    processed_proxies_list = []

    for proxy_string in proxies_list:
        proxy_substrings = proxy_string.split("\t")
        processed_proxies_list.append(proxy_substrings[0] + ":" + proxy_substrings[1] + "\n")

    proxies_file.close()
    proxies_file = open("proxies-list", "w")
    proxies_file.writelines(processed_proxies_list)
    proxies_file.close()

if __name__ == "__main__":
    main()