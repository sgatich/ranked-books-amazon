import threading
from scraper import Scraper

class ScraperWorker(threading.Thread):
    def __init__(self, start_book, end_book):
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self.start_book = start_book
        self.end_book = end_book

    def set_start_end(self, start_book, end_book):
        self.start_book = start_book
        self.end_book = end_book

    def run(self):
        for index in range(self.start_book, self.end_book, 25):
            self.scraper = Scraper()
            self.scraper.run(index, index + 24)
            del self.scraper

        self.scraper = Scraper()
        self.scraper.run(index, self.end_book)
        del self.scraper

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()