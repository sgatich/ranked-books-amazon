from app import mws_api_app, PriceList
from flask import jsonify


@mws_api_app.route("/mws_api/v1.0/<asins_comma>")
def request(asins_comma):
    asins_list = asins_comma.split(",")
    price_list = PriceList()
    price_list.set_asins(asins_list)

    return jsonify(price_list.get_prices())