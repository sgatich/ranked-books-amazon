<!DOCTYPE HTML>
<html lang="en-US">	
	
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>The books from Amazon with the highest rank</title>
        <link href="{{ url_for('static', filename='stylesheets/results.css') }}" rel="stylesheet" type="text/css" />
        <script src="main.js"></script>
    </head>
    <body>
    	<script type="text/javascript" >
    		function showHideFunction(el) 
    		{    	
    			var aobInfoElement = el.nextSibling;
    			
    			alert(aobInfoElement.innerHTML)
    			
    			if (aobInfoElement.style.display === "none") 
    			{
    				aobInfoElement.style.display = "block";    				
    			}    			
    			else 
    			{
    				aobInfoElement.style.display = "none";
    			}
    		}
    	</script>
    	
		{% if current_user.is_authenticated %}
		<div class="row">
		  	<header id="logOut" class="column-12">        	
		  		<a href="/logout" style="float: right;">Log out</a>        	        	
		  	</header>
		</div>
		{% endif %}
        <header id="back">
        	<div class="row column-12">
        		<a href="../index">Back</a>
        	</div>        	
        </header>
        
        <section id="results">
        	{% if pagination %}
			<section class="pagination">
				<ul>
					{% for pagination_element in pagination %}
					{{ pagination_element | safe }}
					{% endfor %}
				</ul>
			</section>
			{% endif %}
			
		{% if books_list %}
		{% for book in books_list %}    			
    		<div class="row">
        		<section class="card column-5">
	        		<section class="cardInfo">	
		        		<section class="bookTitle">
	        				<a href="{{ book.link }}">{{ book.title }}</a>			        				
	        			</section>
		        			
	        			<table class="cardInfoTable">
	        				<tr>
	        					<td>ISBN: </td>
	        					<td>{{ book.isbn }}</td>
	        				</tr>
	        				<tr>
	        					<td>Rank: </td>
	        					<td>{{ book.rank }}</td>
	        				</tr>
	        				<tr>
	        					<td>Profit (like new): </td>
	        					<td>{{ book.profit_like_new }}</td>
	        				</tr>
	        				<tr>
	        					<td>Profit (very good): </td>
	        					<td>{{ book.profit_very_good }}</td>
	        				</tr>
	        				<tr>
	        					<td>Profit ratio: </td>
	        					<td>{{ book.profit_ratio }} %</td>
	        				</tr>
	        				{% if book.trade_in_available %}
	        				<tr>
	        					<td>Trade-in profit: </td>
	        					<td>{{ book.profit_trade_in }}</td>
	        				</tr>
	        				{% endif %}
	        			</table> 
	        		</section>
	        		
	        		{% if book.prices %}
	        		<section class="cardPrices"> 	        			
	        			<table class="cardPricesTable">
	        				<caption>{{ book.cover_type }}: </caption>		
	        				<tr>
	        					{% if book.prices.new %}
	        					<th>New: </th>
	        					{% endif %}
	        					
	        					{% if book.prices.like_new %}
	        					<th>Like new: </th>
	        					{% endif %}
	        					
	        					{% if book.prices.very_good %}
	        					<th>Very good: </th>
	        					{% endif %}
	        					
	        					{% if book.prices.good %}
	        					<th>Good: </th>
	        					{% endif %}
	        					
	        					{% if book.trade_in_available %}
	        					<th>Trade-in:</th>
	        					{% endif %}
	        				</tr>
	        			
	        				{% for price_index in range(5) %}
	        				<tr>
	        					{% if book.prices.new %}
	        					<td>{{ book.prices.new[price_index] }}</td>
	        					{% endif %}
	        					
	        					{% if book.prices.like_new %}
	        					<td>{{ book.prices.like_new[loop.index0] }}</td>
	        					{% endif %}
	        					
	        					{% if book.prices.very_good %}
	        					<td>{{ book.prices.very_good[loop.index0] }}</td>
	        					{% endif %}
	        					
	        					{% if book.prices.good %}
	        					<td>{{ book.prices.good[loop.index0] }}</td>
	        					{% endif %}
	        					
	        					{% if book.trade_in_available %}
	        					<td>{{ book.trade_in_price }}
	        					{% endif %}
	        				</tr>	
	        				{% endfor %}
	        			</table>
	        		</section>
	        		{% else %}
	        		<h3>Error. Cannot render prices.</h3>
	        		{% endif %}
	        		
	        		<section>
	        			<a href="../block_forever/{{ book.isbn }}">Block forever</a><br>
	        			<a href="../block_temporarily/{{ book.isbn }}">Block temporarily</a><br>
	        		</section> 
	        		
	        		{% if book.aob_info %}
	        		<button onclick="showHideFunction(this)">AOB Info</button>
	        		{% endif %}
        		</section>
        		<section class="column-7">
        			
        		</section>
        	</div>
        	
			
			<div class="row">
        		<section class="aobInfo column-5">	        		
	        		{% if book.aob_info %}
	        			        		
	        			{% if book.aob_info.orders %}
	        			<table class="aobOrdersTable">
	        				<tr>
	        					<th>Quantity: </th>			        					
	        					<th>Condition: </th>	
	        					<th>Cost: </th>			        					
	        					<th>Price: </th>
	        					<th>Purchase date: </th>		
	        				</tr>
	        				
	        				{% for order in book.aob_info.orders %}									
							<tr>
	        					<td>{{ order.quantity }}</td>			        					
	        					<td>{{ order.condition }}</td>
	        					<td>{{ order.cost }}</td>
	        					<td>{{ order.price }}</td>
	        					<td>{{ order.purchase_date }}</td>
	        				</tr>
	        				{% endfor %}
	        			</table>
	        			{% endif %}
	        			
	        			{% if book.aob_info.aob_items %}
	        			<table class="aobItemsTable">
	        				<tr>
	        					<th>Quantity: </th>			        					
	        					<th>Condition: </th>		
	        					<th>Cost: </th>		        					
	        					<th>Price: </th>		
	        				</tr>
	        				
	        				{% for item in book.aob_info.aob_items %}									
							<tr>
	        					<td>{{ item.quantity }}</td>			        					
	        					<td>{{ item.condition }}</td>
	        					<td>{{ item.cost }}</td>
	        					<td>{{ item.price }}</td>
	        				</tr>
	        				{% endfor %}
	        			</table>
	        			{% endif %}	        			
	        		
	        		{% endif %}  		
        		</section>
        		
        		<section class="column-7">
        			
        		</section>
        	</div>
		{% endfor %}	        	
    	{% endif %}
    	
    	{% if pagination %}
			<section class="pagination">
				<ul>
					{% for pagination_element in pagination %}
					{{ pagination_element | safe }}
					{% endfor %}
				</ul>
			</section>
		{% endif %}
        </section>
    </body>
</html>