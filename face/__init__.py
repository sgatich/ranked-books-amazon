from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

faceApp = Flask(__name__, template_folder="templates")
faceApp.config.from_object(Config)

login = LoginManager(faceApp)
login.login_view = "login"

db = SQLAlchemy(faceApp)


from face import views