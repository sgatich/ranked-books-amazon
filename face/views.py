from face import faceApp
from flask import render_template, redirect, url_for, session, request
from .forms import *
from models import *
from sqlalchemy import literal
from sqlalchemy import or_
from datetime import datetime, timedelta
from flask_login import current_user, login_user, logout_user, login_required
from face.flask_models import User
from face.paginator import Paginator


faceApp.secret_key = "s8%Wb5:qLT41"


@faceApp.before_request
def session_management():
    session.permanent = True


@faceApp.route("/", methods=["GET", "POST"])
@faceApp.route("/index", methods=["GET", "POST"])
@login_required
def index():
    form = BooksQueryForm()

    if form.validate_on_submit():
        if form.title.data != "":
            session["title"] = form.title.data

        if form.isbn.data != "":
            session["isbn"] = form.isbn.data

        if form.rank.data != "":
            try:
                rank = int(form.rank.data)
            except:
                return render_template("index.html", form=form)
            session["rank"] = rank

        if form.profit.data != "":
            try:
                profit = float(form.profit.data)
            except:
                return render_template("index.html", form=form)
            session["profit"] = profit

        if form.profit_ratio.data != "":
            try:
                profit_ratio = float(form.profit_ratio.data)
            except:
                return render_template("index.html", form=form)
            session["profit_ratio"] = profit_ratio

        if form.profit_trade_in.data != "":
            try:
                profit_trade_in = float(form.profit_trade_in.data)
            except:
                return render_template("index.html", form=form)
            session["profit_trade_in"] = profit_trade_in

        session["show_new"] = form.show_new.data
        session["show_like_new"] = form.show_like_new.data
        session["show_very_good"] = form.show_very_good.data
        session["show_good"] = form.show_good.data

        session["newer_unavailable"] = form.newer_unavailable.data
        session["only_trade_in"] = form.only_trade_in.data
        session["category"] = dict(form.category.choices).get(form.category.data)

        print("Chosen category: " + session["category"])
        # print("Number of books in the list: " + str(len(session["books_list"])))

        return redirect("/results/1")
        # return render_template("results.html", books_list=books_list)

    current_bd_session = Session()
    categories = current_bd_session.query(Category).order_by(Category.name)
    form.category.choices += [(category.name.lower(), category.name) for category in categories]
    return render_template("index.html", form=form)


@faceApp.route("/results/<int:page>")
@login_required
def results(page):
    print("Page #" + str(page))
    per_page = 10

    title = ""
    isbn = ""
    rank = None
    profit = None
    profit_ratio = None
    profit_trade_in = None
    newer_unavailable = True
    only_trade_in = False
    category = ""
    conditions = {}

    if "title" in session:
        title = session["title"]
    if "isbn" in session:
        isbn = session["isbn"]
    if "category" in session:
        category = session["category"]
    if "rank" in session:
        rank = session["rank"]
    if "profit" in session:
        profit = session["profit"]
    if "profit_ratio" in session:
        profit_ratio = session["profit_ratio"]
    if "profit_trade_in" in session:
        profit_trade_in = session["profit_trade_in"]
    if "newer_unavailable" in session:
        newer_unavailable = session["newer_unavailable"]
    if "only_trade_in" in session:
        only_trade_in = session["only_trade_in"]
    if "show_new" in session:
        conditions["show_new"] = session["show_new"]
    if "show_like_new" in session:
        conditions["show_like_new"] = session["show_like_new"]
    if "show_very_good" in session:
        conditions["show_very_good"] = session["show_very_good"]
    if "show_good" in session:
        conditions["show_good"] = session["show_good"]

    query_books = form_query_books(title,
                                   isbn,
                                   rank,
                                   profit,
                                   profit_ratio,
                                   profit_trade_in,
                                   only_trade_in,
                                   category,
                                   newer_unavailable)

    query_books_number = query_books.count()
    paginator = Paginator(query_books_number, page)
    pagination = paginator.get_html_elements()
    print("Pagination:")
    print(pagination)

    start = page * per_page - 10
    end = start + per_page

    query_results = query_books[start:end]
    books_list = form_books_list(query_results, conditions)
    print("Number of books in query_books: " + str(query_books.count()))

    return render_template("results.html", books_list=books_list, pagination=pagination)


@faceApp.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        print(user)

        if user is None or not user.check_password(form.password.data):
            error_message = "Invalid username or password"
            return render_template("login.html", form=form, error_message=error_message)
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for("index"))

    print(form.errors)
    print(form.validate_on_submit())

    return render_template("login.html", form=form)


@faceApp.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))


@faceApp.route("/block_temporarily/<isbn>")
@login_required
def block_temporarily(isbn):
    current_bd_session = Session()

    current_moment = datetime.now()
    two_days = timedelta(days=2)

    print("I'll block this ISBN temporarily: " + isbn)

    book_entry = current_bd_session.query(Book).filter(Book.isbn == isbn).first()
    book_entry.block_until = current_moment + two_days
    current_bd_session.add(book_entry)
    current_bd_session.commit()

    return redirect("/results/1")


@faceApp.route("/block_forever/<isbn>")
@login_required
def block_forever(isbn):
    current_bd_session = Session()

    far_future = datetime(3000, 1, 1)

    print("I'll block this ISBN temporarily: " + isbn)

    book_entry = current_bd_session.query(Book).filter(Book.isbn == isbn).first()
    book_entry.block_until = far_future
    current_bd_session.add(book_entry)
    current_bd_session.commit()

    return redirect("/results/1")


@faceApp.route("/add_crook", methods=["GET", "POST"])
def add_crook():
    form = AddCrooksForm()

    crooks = []

    current_bd_session = Session()
    crooks_entries = current_bd_session.query(Crook).all()

    for entry in crooks_entries:
        crook_dict = {}
        print("Name: " + entry.name)

        crook_dict["name"] = entry.name
        crook_dict["reason"] = entry.reason
        crooks.append(crook_dict)

    if not form.validate_on_submit():
        return render_template("add_crook.html", form=form, crooks=crooks)

    if form.validate_on_submit():

        seller = form.name.data
        reason = form.reason.data

        print(seller)
        print(reason)

        if seller != "":
            crook_entry = Crook(name=seller, reason=reason)
            current_bd_session.add(crook_entry)
            current_bd_session.commit()

        return redirect("/add_crook")

    return render_template("add_crook.html", form=form, crooks=crooks)


@faceApp.route("/remove_crook", methods=["GET", "POST"])
def remove_crook():
    form = RemoveCrooksForm()

    if not form.validate_on_submit():
        return render_template("remove_crook.html", form=form)

    if form.validate_on_submit():
        current_bd_session = Session()
        seller = form.name.data

        if seller != "":
            crook_entries = current_bd_session.query(Crook).filter(Crook.name == seller).all()
            for crook_entry in crook_entries:
                current_bd_session.delete(crook_entry)
                current_bd_session.commit()

        return redirect("/remove_crook")

    return render_template("remove_crook.html", form=form)


def form_prices(price_entries, conditions):
    prices_dict = {}
    prices_dict = form_prices_cover(price_entries)

    if not conditions["show_new"]:
        del prices_dict["new"]
    if not conditions["show_like_new"]:
        del prices_dict["like_new"]
    if not conditions["show_very_good"]:
        del prices_dict["very_good"]
    if not conditions["show_good"]:
        del prices_dict["good"]

    return prices_dict


def form_prices_cover(price_entries):
    cover_prices = {}
    cover_prices["new"] = []
    cover_prices["like_new"] = []
    cover_prices["very_good"] = []
    cover_prices["good"] = []

    for price_entry in price_entries:
        if price_entry.condition == "New":
            cover_prices["new"].append(str(price_entry.price) + "+" + str(price_entry.shipping))
        elif price_entry.condition == "Like new":
            cover_prices["like_new"].append(str(price_entry.price) + "+" + str(price_entry.shipping))
        elif price_entry.condition == "Very good":
            cover_prices["very_good"].append(str(price_entry.price) + "+" + str(price_entry.shipping))
        elif price_entry.condition == "Good":
            cover_prices["good"].append(str(price_entry.price) + "+" + str(price_entry.shipping))

    if len(cover_prices["new"]) >= 5:
        cover_prices["new"] = cover_prices["new"][:5]
    else:
        for i in range(len(cover_prices["new"]), 5):
            cover_prices["new"].append("N/A")

    if len(cover_prices["like_new"]) >= 5:
        cover_prices["like_new"] = cover_prices["like_new"][:5]
    else:
        for i in range(len(cover_prices["like_new"]), 5):
            cover_prices["like_new"].append("N/A")

    if len(cover_prices["very_good"]) >= 5:
        cover_prices["very_good"] = cover_prices["very_good"][:5]
    else:
        for i in range(len(cover_prices["very_good"]), 5):
            cover_prices["very_good"].append("N/A")

    if len(cover_prices["good"]) >= 5:
        cover_prices["good"] = cover_prices["good"][:5]
    else:
        for i in range(len(cover_prices["good"]), 5):
            cover_prices["good"].append("N/A")

    return cover_prices


def form_query_books(title, isbn, rank, profit, profit_ratio, profit_trade_in, only_trade_in, category, newer_unavailable):
    current_bd_session = Session()
    query_books = current_bd_session.query(Book).order_by(Book.id)

    if title != "":
        query_books = query_books.filter(Book.title.ilike("%" + title + "%"))
    if isbn != "":
        query_books = query_books.filter(Book.isbn.ilike("%" + title + "%"))
    if rank:
        query_books = query_books.filter(Book.rank <= rank)
    if profit:
        query_books = query_books.filter(or_(Book.profit_like_new >= profit,
                                             Book.profit_very_good >= profit))
    if profit_ratio:
        query_books = query_books.filter(Book.profit_ratio >= profit_ratio)
    if profit_trade_in:
        query_books = query_books.filter(Book.trade_in_available == True)
        query_books = query_books.filter(Book.profit_trade_in >= profit_trade_in)
    if only_trade_in:
        query_books = query_books.filter(Book.trade_in_available == True)

    query_books = query_books.filter(Book.newer_edition_available != newer_unavailable)

    current_moment = datetime.now()
    query_books = query_books.filter(Book.block_until <= current_moment)

    if category != "" and category != "All":
        print("Filtering by category.")
        query_books = query_books.join(Category).filter(Category.name == category)

    return query_books


def form_books_list(query_results, conditions):
    books_list = []

    for bookDb in query_results:
        aob_info = form_aob_info(bookDb)

        book = {}
        book["title"] = bookDb.title
        book["isbn"] = bookDb.isbn
        book["link"] = bookDb.link
        book["rank"] = "{:,}".format(bookDb.rank)
        price_entries = bookDb.prices

        book_prices = form_prices(price_entries, conditions)

        book["prices"] = book_prices
        book["profit_like_new"] = bookDb.profit_like_new
        book["profit_very_good"] = bookDb.profit_very_good
        profit_ratio = bookDb.profit_ratio
        profit_ratio *= 100
        profit_ratio = "{:4.3f}".format(profit_ratio)
        book["profit_ratio"] = profit_ratio

        book["profit_trade_in"] = bookDb.profit_trade_in
        book["cover_type"] = bookDb.cover_type
        book["trade_in_available"] = bookDb.trade_in_available
        book["trade_in_price"] = bookDb.trade_in_price

        book["aob_info"] = aob_info

        books_list.append(book)

    return books_list


def form_aob_info(book_entry):
    aob_info = {}
    current_bd_session = Session()

    aob_orders = []
    order_entries = current_bd_session.query(AOBOrder).filter(AOBOrder.isbn == book_entry.isbn)[:]

    for order_entry in order_entries:
        aob_order = {}
        aob_order["quantity"] = order_entry.qty

        if order_entry.condition == "11":
            aob_order["condition"] = "New"
        elif order_entry.condition == "1":
            aob_order["condition"] = "Like new"
        elif order_entry.condition == "2":
            aob_order["condition"] = "Very good"
        elif order_entry.condition == "3":
            aob_order["condition"] = "Good"
        else:
            aob_order["condition"] = ""

        aob_order["cost"] = order_entry.cost
        aob_order["price"] = order_entry.price
        aob_order["purchase_date"] = order_entry.last_update.split(" ")[0]
        aob_orders.append(aob_order)

    if len(aob_orders) > 0:
        aob_info["orders"] = aob_orders

    aob_items = []
    item_entries = current_bd_session.query(AOBItem).filter(AOBItem.isbn == book_entry.isbn)[:]

    for item_entry in item_entries:
        aob_item = {}
        aob_item["quantity"] = item_entry.qty

        if item_entry.condition == "11":
            aob_item["condition"] = "New"
        elif item_entry.condition == "1":
            aob_item["condition"] = "Like new"
        elif item_entry.condition == "2":
            aob_item["condition"] = "Very good"
        elif item_entry.condition == "3":
            aob_item["condition"] = "Good"
        else:
            aob_item["condition"] = ""

        aob_item["cost"] = item_entry.cost

        aob_item["price"] = item_entry.price
        aob_items.append(aob_item)

    if len(aob_items) > 0:
        aob_info["aob_items"] = aob_items

    return aob_info
