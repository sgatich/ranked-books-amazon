class Paginator:
    def __init__(self, number_results, chosen_page):
        self.number_results = number_results
        self.chosen_page = chosen_page
        self.number_pages = 0
        self.result_list = []
        self.type = ""

    def get_html_elements(self):
        self.calculate_number_pages()
        print("Pages: " + str(self.number_pages))

        if self.chosen_page > self.number_pages:
            return self.result_list

        self.choose_type()
        self.form_result()
        return self.result_list

    def calculate_number_pages(self):
        self.number_pages = self.number_results // 10
        if self.number_results % 10 != 0:
            self.number_pages += 1

    def choose_type(self):
        if self.number_pages <= 5:
            self.type = "six_or_less"
        else:
            self.type = "more_six"

    def form_result(self):
        if self.type == "six_or_less":
            self.form_six_or_less()
        elif self.type == "more_six":
            self.form_more_six()
        else:
            print("Unknown type")

    def form_six_or_less(self):
        if self.chosen_page == 1:
            self.result_list.append("<li>Previous</li>")
        else:
            self.result_list.append('<li><a href="/results/' + str(self.chosen_page - 1) + '">Previous</a></li>')

        for i in range(1, self.number_pages + 1):
            if self.chosen_page == i:
                self.result_list.append('<li>' + str(i) + '</li>')
            else:
                self.result_list.append('<li><a href="/results/' + str(i) + '">' + str(i) + '</a></li>')

        if self.chosen_page == self.number_pages:
            self.result_list.append("<li>Next</li>")
        else:
            self.result_list.append('<li><a href="/results/' + str(self.chosen_page + 1) + '">Next</a></li>')

    def form_more_six(self):
        if self.chosen_page in range(1, 4):
            self.form_more_six_beginning()
        elif self.chosen_page in range(self.number_pages - 2, self.number_pages + 1):
            self.form_more_six_end()
        else:
            self.form_more_six_middle()

    def form_more_six_beginning(self):
        if self.chosen_page == 1:
            pass
        else:
            self.result_list.append('<li><a href="/results/' + str(self.chosen_page - 1) + '">Previous</a></li>')

        for i in range(1, 5):
            if self.chosen_page == i:
                self.result_list.append('<li>' + str(i) + '</li>')
            else:
                self.result_list.append('<li><a href="/results/' + str(i) + '">' + str(i) + '</a></li>')

        self.result_list.append("<li>...</li>")
        self.result_list.append('<li><a href="/results/' + str(self.number_pages) + '">' + str(self.number_pages) + '</a></li>')
        self.result_list.append('<li><a href="/results/' + str(self.chosen_page + 1) + '">Next</a></li>')

    def form_more_six_middle(self):
        self.result_list.append('<li><a href="/results/' + str(self.chosen_page - 1) + '">Previous</a></li>')
        self.result_list.append('<li><a href="/results/' + str(1) + '">' + str(1) + '</a></li>')
        self.result_list.append("<li>...</li>")

        for i in range(self.chosen_page - 1, self.chosen_page + 2):
            if self.chosen_page == i:
                self.result_list.append('<li>' + str(i) + '</li>')
            else:
                self.result_list.append('<li><a href="/results/' + str(i) + '">' + str(i) + '</a></li>')

        self.result_list.append("<li>...</li>")
        self.result_list.append('<li><a href="/results/' + str(self.number_pages) + '">' + str(self.number_pages) + '</a></li>')
        self.result_list.append('<li><a href="/results/' + str(self.chosen_page + 1) + '">Next</a></li>')

    def form_more_six_end(self):
        self.result_list.append('<li><a href="/results/' + str(self.chosen_page - 1) + '">Previous</a></li>')
        self.result_list.append('<li><a href="/results/' + str(1) + '">' + str(1) + '</a></li>')
        self.result_list.append("<li>...</li>")

        for i in range(self.number_pages - 3, self.number_pages + 1):
            if self.chosen_page == i:
                self.result_list.append('<li>' + str(i) + '</li>')
            else:
                self.result_list.append('<li><a href="/results/' + str(i) + '">' + str(i) + '</a></li>')

        if self.chosen_page == self.number_pages:
            pass
        else:
            self.result_list.append('<li><a href="/results/' + str(self.chosen_page + 1) + '">Next</a></li>')


