from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, BooleanField, PasswordField
from wtforms.validators import DataRequired


class BooksQueryForm(FlaskForm):
    title = StringField("inputTitle")
    isbn = StringField("inputIsbn")

    newer_unavailable = BooleanField("inputNewer")
    only_trade_in = BooleanField("inputTradeIn")
    show_new = BooleanField("inputShowNew", default="checked")
    show_like_new = BooleanField("inputShowLikeNew", default="checked")
    show_very_good = BooleanField("inputShowVeryGood", default="checked")
    show_good = BooleanField("inputShowGood", default="checked")

    rank = StringField("inputRank")
    profit = StringField("inputProfit")
    profit_ratio = StringField("inputProfitRatio")
    profit_trade_in = StringField("inputTradeIn")
    category = SelectField("inputCategory", choices=[("all", "All")])


class LoginForm(FlaskForm):
    username = StringField("inputUsername", validators=[DataRequired()])
    password = PasswordField("inputPassword", validators=[DataRequired()])
    remember_me = BooleanField("inputRememberMe")


class AddCrooksForm(FlaskForm):
    name = StringField("addInputName")
    reason = StringField("addInputReason")


class RemoveCrooksForm(FlaskForm):
    name = StringField("removeInputName")


class FindCrooksForm(FlaskForm):
    name = StringField("findInputName")