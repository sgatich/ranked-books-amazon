from ftplib import FTP


class AOBConnector:
    def __init__(self):
        self.ftp_client = FTP("ftp.theartofbooks.com", user="medianeat", passwd="kniga123")

    def get_files(self):
        self.ftp_client.cwd("downloads")
        self.ftp_client.retrbinary("RETR regular_inv.txt", open("regular_inv.txt", "wb").write)
        print("I've received the first file.")
        self.ftp_client.retrbinary("RETR regular_orders.txt", open("regular_orders.txt", "wb").write)
        print("I've received the second file.")