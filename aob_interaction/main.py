from aobconnector import AOBConnector
from aob_parser import AOBParser



def main():
    # art_of_books_ftp = AOBConnector()
    # art_of_books_ftp.get_files()

    art_of_books_parser = AOBParser()
    art_of_books_parser.run()


if __name__ == "__main__":
    main()