from database import Database


class AOBParser:
    def __init__(self):
        self.database = Database()

    def run(self):
        # self.parse_inventory()
        self.parse_orders()

    def parse_inventory(self):
        print("====================================================")
        print("I'm parsing inventory.")
        print("====================================================")
        inventory_file = open("regular_inv.txt", "r")
        inventory = inventory_file.readlines()

        headers = inventory[0].split("\t")
        for header in headers:
            print(header)

        for item in inventory[1:]:
            inventory_fields = item.split("\t")
            print(inventory_fields)

            try:
                self.database.save_item(inventory_fields)
            except:
                print("Cannot save this item.")
                print(item)

            # for i in range(0, len(inventory_fields)):
            #     print(headers[i])
            #     print(inventory_fields[i])
            #     print("========================================")

        # for item in inventory[:3]:
        #     print(item)

    def parse_orders(self):
        print("====================================================")
        print("I'm parsing the orders.")
        print("====================================================")
        orders_file = open("regular_orders.txt", "r")
        orders = orders_file.readlines()

        headers = orders[0].split("\t")
        for header in headers:
            print(header)

        for order in orders[1:]:
            order_fields = order.split("\t")
            print(order_fields)
            try:
                self.database.save_order(order_fields)
            except:
                print("Cannot save this order.")
                print(order)
            # for i in range(0, len(order_fields)):
            #     print(headers[i])
            #     print(order_fields[i])
            #     print("========================================")