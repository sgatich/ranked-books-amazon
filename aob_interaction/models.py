from sqlalchemy import create_engine
from sqlalchemy.schema import MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, BigInteger, Float, Boolean, String, Text, Date, ForeignKey, DateTime
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy.orm import scoped_session


engine = create_engine("postgresql://gsa:pass@localhost/amazon_books_dev", echo=False, pool_size=30)

SessionFactory = sessionmaker(bind=engine)
Session = scoped_session(SessionFactory)

metadata = MetaData()
Base = declarative_base()


class Category(Base):
    __tablename__ = "categories"
    id = Column(Integer, primary_key=True)
    name = Column(String(length=100))
    link = Column(String(length=300))

    links_to_books = relationship("BookLink", back_populates="category")
    books = relationship("Book", back_populates="category")


class BookLink(Base):
    __tablename__ = "links_to_books"
    id = Column(Integer, primary_key=True)
    link = Column(String(length=400), unique=True)
    date = Column(Date)

    category_id = Column(Integer, ForeignKey("categories.id"))
    category = relationship("Category", back_populates="links_to_books")


class BookLinkFiltered(Base):
    __tablename__ = "links_filtered"
    id = Column(Integer, primary_key=True)
    link = Column(String(length=400), unique=True)
    date = Column(Date)


class Proxy(Base):
    __tablename__ = "proxies"
    id = Column(Integer, primary_key=True)
    proxy = Column(String(length=100))


class CompromisedProxy(Base):
    __tablename__ = "compromised_proxies"
    id = Column(Integer, primary_key=True)
    proxy = Column(String(length=100))
    times = Column(Integer)


class Crook(Base):
    __tablename__ = "crooks"
    id = Column(Integer, primary_key=True)
    name = Column(String(length=300))
    reason = Column(String(length=500))


class Book(Base):
    __tablename__ = "books"
    id = Column(Integer, primary_key=True)

    link = Column(String(length=300), unique=True)
    asin = Column(String(length=60))

    title = Column(String(length=400))
    isbn = Column(String(length=50))

    newer_edition_available = Column(Boolean)
    newer_edition_link = Column(String(length=150))

    trade_in_available = Column(Boolean)
    trade_in_price = Column(Float)

    rank = Column(BigInteger)
    cover_type = Column(String(length=100))

    profit_like_new = Column(Float)
    profit_very_good = Column(Float)
    profit_ratio = Column(Float)
    profit_trade_in = Column(Float)

    block_until = Column(DateTime)

    category_id = Column(Integer, ForeignKey("categories.id"))
    category = relationship("Category", back_populates="books")


class Price(Base):
    __tablename__ = "prices"
    id = Column(Integer, primary_key=True)
    price = Column(Float)
    shipping = Column(Float)
    condition = Column(String(length=100))
    sellers_rating = Column(String(length=100))
    timestamp = Column(DateTime)

    book_id = Column(Integer, ForeignKey("books.id"))
    book = relationship("Book", back_populates="prices")


class Statistic(Base):
    __tablename__ = "statistics"
    id = Column(Integer, primary_key=True)
    average = Column(Float)
    minimum = Column(Float)
    maximum = Column(Float)
    timestamp = Column(DateTime)

    book_id = Column(Integer, ForeignKey("books.id"))
    book = relationship("Book", back_populates="statistics")


class MwsApiAccount(Base):
    __tablename__ = "mws_api_accounts"
    id = Column(Integer, primary_key=True)


class ProductApiAccount(Base):
    __tablename__ = "product_api_accounts"
    id = Column(Integer, primary_key=True)

    associate_tag = Column(String(length=150))
    access_key_id = Column(String(length=200))
    secret_access_key = Column(String(length=500))

    last_updated = Column(DateTime)


class AOBOrder(Base):
    __tablename__ = "aob_orders"

    id = Column(Integer, primary_key=True)

    abe_book_id = Column(Text)
    abe_cc = Column(Text)
    abe_ccexp = Column(Text)
    abe_ccname = Column(Text)
    abe_customer_id = Column(Text)
    abe_domain_id = Column(Text)
    abe_domain_name = Column(Text)
    abe_gst = Column(Text)
    abe_handling = Column(Text)
    abe_max_delivery_days = Column(Text)
    abe_min_delivery_days = Column(Text)
    abe_order_date = Column(Text)
    abe_order_item_status_code = Column(Text)
    abe_order_item_status_name = Column(Text)
    abe_purchase_method = Column(Text)
    abe_reseller_id = Column(Text)
    abe_reseller_name = Column(Text)
    abe_seller_id = Column(Text)
    abe_seller_total = Column(Text)
    abe_shipment_mainfest = Column(Text)
    abe_shipping = Column(Text)
    abe_shipping_company = Column(Text)
    abe_shipping_extra_item_cost = Column(Text)
    abe_shipping_first_item_cost = Column(Text)
    abe_shipping_tracking_code = Column(Text)
    abe_special_instructions = Column(Text)
    abe_status_code = Column(Text)
    abe_status_name = Column(Text)
    abe_subtotal = Column(Text)
    abe_tax = Column(Text)
    abe_total = Column(Text)
    actual_shipping_cost = Column(Text)
    ali_list_price = Column(Text)
    ali_payable = Column(Text)
    ali_ship_payment = Column(Text)
    aob_comments = Column(Text)
    aob_pull_time = Column(Text)
    aob_status = Column(Text)
    author = Column(Text)
    batch_id = Column(Text)
    buyer_email = Column(Text)
    buyer_name = Column(Text)
    buyer_phone = Column(Text)
    comments = Column(Text)
    commission = Column(Text)
    condition = Column(Text)
    confirm_status = Column(Text)
    core_price = Column(Text)
    cost = Column(Text)
    dc = Column(Text)
    eby_buyer_selected_shipping = Column(Text)
    eby_checkout_status = Column(Text)
    eby_complete_status = Column(Text)
    eby_last_mod_time = Column(Text)
    eby_payment_method_used = Column(Text)
    eby_payment_status = Column(Text)
    handling = Column(Text)
    hb_catalog = Column(Text)
    image = Column(Text)
    insurance_amount = Column(Text)
    insurance_type = Column(Text)
    isbn = Column(Text)
    item_name = Column(Text)
    item_price = Column(Text)
    last_update = Column(Text)
    listed_date = Column(Text)
    listing_id = Column(Text)
    location = Column(Text)
    matched = Column(Text)
    media = Column(Text)
    mskus = Column(Text)
    notes = Column(Text)
    order_id = Column(Text)
    order_item_id = Column(Text, unique=True)
    order_items = Column(Text)
    order_processing_fee = Column(Text)
    payment_date = Column(Text)
    payment_transaction_id = Column(Text)
    payments_status = Column(Text)
    price = Column(Text)
    processing_fee = Column(Text)
    publisher = Column(Text)
    purchase_date = Column(Text)
    qty = Column(Text)
    recipient_name = Column(Text)
    release_date = Column(Text)
    rental = Column(Text)
    rental_price = Column(Text)
    return_date = Column(Text)
    ship_address_1 = Column(Text)
    ship_address_2 = Column(Text)
    ship_address_3 = Column(Text)
    ship_city = Column(Text)
    ship_country = Column(Text)
    ship_method = Column(Text)
    ship_state = Column(Text)
    ship_zip = Column(Text)
    shipped_date = Column(Text)
    shipped_method = Column(Text)
    shipping = Column(Text)
    shipping_fee = Column(Text)
    shipping_weight = Column(Text)
    sku = Column(Text)
    source = Column(Text)
    stripped_title = Column(Text)
    tax = Column(Text)
    title = Column(Text)
    total = Column(Text)
    total_price = Column(Text)
    upc = Column(Text)
    venue = Column(Text)
    weight = Column(Text)


class AOBItem(Base):
    __tablename__ = "aob_inventory"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)

    ali_status = Column(Text)
    asin1 = Column(Text)
    asin2 = Column(Text)
    asin3 = Column(Text)
    author = Column(Text)
    autoreprice_exclude = Column(Text)
    cond_text = Column(Text)
    condition = Column(Text)
    cost = Column(Text)
    description = Column(Text)
    ebay_attributes = Column(Text)
    eby_add_sent = Column(Text)
    eby_cat1 = Column(Text)
    eby_cat2 = Column(Text)
    eby_duration = Column(Text)
    eby_expid = Column(Text)
    eby_fees = Column(Text)
    eby_item_id = Column(Text)
    eby_list_time = Column(Text)
    eby_type = Column(Text)
    edition = Column(Text)
    expd = Column(Text)
    fba = Column(Text)
    flags = Column(Text)
    flex = Column(Text)
    fnsku = Column(Text)
    free_shipping = Column(Text)
    hb_bind = Column(Text)
    hb_catalog = Column(Text)
    hb_sub1 = Column(Text)
    hlf_expid = Column(Text)
    hlf_item_id = Column(Text)
    illustrator = Column(Text)
    image = Column(Text)
    intl = Column(Text)
    isbn = Column(Text)
    jcond_text = Column(Text)
    keywords = Column(Text)
    last_eby_item_id = Column(Text)
    last_hlf_item_id = Column(Text)
    last_update = Column(Text)
    list_flags = Column(Text)
    listed_date = Column(Text)
    location = Column(Text)
    media = Column(Text)
    notes = Column(Text)
    p0 = Column(Text)
    p1 = Column(Text)
    p2 = Column(Text)
    p3 = Column(Text)
    p4 = Column(Text)
    p5 = Column(Text)
    p6 = Column(Text)
    p7 = Column(Text)
    p8 = Column(Text)
    p9 = Column(Text)
    presoloist_price = Column(Text)
    price = Column(Text)
    price_a1b = Column(Text)
    price_abe = Column(Text)
    price_ali = Column(Text)
    price_amc = Column(Text)
    price_amd = Column(Text)
    price_amf = Column(Text)
    price_amu = Column(Text)
    price_amz = Column(Text)
    price_bib = Column(Text)
    price_bnn = Column(Text)
    price_chb = Column(Text)
    price_chr = Column(Text)
    price_cs0 = Column(Text)
    price_cs1 = Column(Text)
    price_cs2 = Column(Text)
    price_cs3 = Column(Text)
    price_cs4 = Column(Text)
    price_cs5 = Column(Text)
    price_cs6 = Column(Text)
    price_cs7 = Column(Text)
    price_cs8 = Column(Text)
    price_cs9 = Column(Text)
    price_eby = Column(Text)
    price_eca = Column(Text)
    price_elm = Column(Text)
    price_hfe = Column(Text)
    price_hlf = Column(Text)
    price_src = Column(Text)
    price_tbx = Column(Text)
    price_tru = Column(Text)
    price_val = Column(Text)
    product_id_type = Column(Text)
    pub_place = Column(Text)
    publisher = Column(Text)
    qty = Column(Text)
    release_date = Column(Text)
    rental = Column(Text)
    reprice_flags = Column(Text)
    sales_rank = Column(Text)
    signature = Column(Text)
    size = Column(Text)
    sku = Column(Text)
    source = Column(Text)
    stripped_title = Column(Text)
    title = Column(Text)
    upc = Column(Text)
    volume = Column(Text)
    weight = Column(Text)
    z_browse = Column(Text)
    z_category = Column(Text)
    z_shipping = Column(Text)
    z_storefront = Column(Text)


Book.prices = relationship("Price", order_by=Price.id, back_populates="book")
Book.statistics = relationship("Statistic", order_by=Statistic.id, back_populates="book")


Base.metadata.create_all(engine)