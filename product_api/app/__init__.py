import bottlenose
import time
from bs4 import BeautifulSoup
from flask import Flask


product_api_app = Flask(__name__)

class BooksInfo:
    def __init__(self):
        self.amazon = bottlenose.Amazon("AKIAI3IHII5JDRKSVXOA",
                                        "qTwkNTFJxZQI5RsGJ3+VTdsu5BgWLTqE23ZMKcuq",
                                        "petsprostor09-20")
        self.info = {}

    def get_info(self):
        info_result = self.info.copy()
        self.info.clear()
        self.info = {}

        return info_result

    def process_asin(self, asin):
        response = self.make_request_to_api(asin)

        sales_rank = self.get_sales_rank(response)
        cover_type = self.get_cover_type(response)
        isbn = self.get_isbn(response)
        title = self.get_title(response)

        parameters_valid = self.check_info(sales_rank, cover_type)
        if not parameters_valid:
            return False

        self.save_info(sales_rank, cover_type, title, isbn)
        return True

    def make_request_to_api(self, asin):
        response = ""

        while True:
            print("For ASIN: " + asin)
            try:
                response = self.amazon.ItemLookup(ItemId=asin, ResponseGroup="SalesRank,ItemAttributes")
            except:
                time.sleep(1)
                continue
            break

        return response

    def get_sales_rank(self, response):
        soup = BeautifulSoup(response, "xml")

        sales_ranks = soup.select("SalesRank")
        sales_rank = 1000000
        if len(sales_ranks) > 0:
            sales_rank = int(sales_ranks[0].get_text())

        return sales_rank

    def get_cover_type(self, response):
        soup = BeautifulSoup(response, "xml")

        cover_types = soup.select("Binding")
        cover_type = ""
        if len(cover_types) > 0:
            cover_type = cover_types[0].get_text()

        return cover_type

    def get_isbn(self, response):
        soup = BeautifulSoup(response, "xml")

        isbns = soup.select("ISBN")
        isbn = ""
        if len(isbns) > 0:
            isbn = isbns[0].get_text()

        return isbn

    def get_title(self, response):
        soup = BeautifulSoup(response, "xml")

        titles = soup.select("Title")
        title = ""
        if len(titles) > 0:
            title = titles[0].get_text()

        return title

    def check_info(self, sales_rank, cover_type):
        if sales_rank > 300000:
            return False

        if cover_type != "Hardcover" and cover_type != "Paperback":
            return False

        return True

    def save_info(self, sales_rank, cover_type, title, isbn):
        self.info["sales_rank"] = sales_rank
        self.info["cover_type"] = cover_type
        self.info["title"] = title
        self.info["isbn"] = isbn


from app import views
