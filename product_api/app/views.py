from app import product_api_app, BooksInfo
from flask import jsonify


@product_api_app.route("/product_api/v1.0/<asin>")
def request(asin):
    books_info = BooksInfo()

    is_processed = books_info.process_asin(asin)
    if is_processed:
        return jsonify(books_info.get_info())
    else:
        return jsonify({"error": "The product doesn't fit."})
