import threading
from scraper import Scraper


class LinksWorker(threading.Thread):
    def __init__(self, start_category, end_category):
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self.start_category = start_category
        self.end_category = end_category

    def set_start_end(self, start_book, end_book):
        self.start_category = start_book
        self.end_category = end_book

    def run(self):
        for index in range(self.start_category, self.end_category, 25):
            self.scraper = Scraper()
            self.scraper.obtain_most_selling(index, index + 24)
            del self.scraper

        self.scraper = Scraper()
        self.scraper.obtain_most_selling(index, self.end_category)
        del self.scraper

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()