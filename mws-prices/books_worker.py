import threading
from datetime import datetime
from books_controller import BooksController

class BooksWorker(threading.Thread):
    def __init__(self, start_book, end_book):
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self.start_book = start_book
        self.end_book = end_book

    def set_start_end(self, start_book, end_book):
        self.start_book = start_book
        self.end_book = end_book

    def run(self):
        t1 = datetime.now()
        self.scraper = BooksController()
        self.scraper.run(self.start_book, self.end_book)

        t2 = datetime.now()

        td = t2 - t1
        total_time = td.total_seconds()

        # print "Seconds for general information: " + str(self.scraper.get_info_operation())
        # print "Seconds for trade-in and newer edition: " + str(self.scraper.get_trade_in_newer_operation())
        # print "Seconds for the prices: " + str(self.scraper.get_prices_operation())
        print "Seconds total: " + str(total_time)

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()