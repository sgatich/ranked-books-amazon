from datetime import datetime
from models import *


class Storage:
    def __init__(self):
        self.session = Session()
        metadata.create_all(bind=engine)
        self.most_selling_dict = {}

    def save_books(self, info_dict, prices_dict, profits_dict, links_dict, statistics_dict):
        for asin in info_dict.keys():
            book_link = links_dict[asin]["link"]
            category = links_dict[asin]["category"]
            book_entry = self.get_or_create_book_entry(info_dict[asin], profits_dict[asin], book_link, asin)
            price_entries = self.make_price_entries(prices_dict[asin])
            statistics_entry = self.make_statistics_entry(statistics_dict[asin])

            book_entry.category = category
            book_entry.prices.extend(price_entries)
            book_entry.statistics.append(statistics_entry)
            self.session.add(book_entry)
            self.session.commit()

            # try:
            #     self.session.commit()
            # except:
            #     print("Error. Maybe the database already has this book.")
            #     self.session.rollback()

    def make_price_entries(self, prices):
        price_entries = []

        for price_dict in prices:
            subcondition = price_dict["subcondition"]
            price = float(price_dict["price"])
            shipping = float(price_dict["shipping"])
            sellers_rating = price_dict["sellers_rating"]
            timestamp = datetime.now()

            price_entry = Price(price=price,
                                shipping=shipping,
                                condition=subcondition,
                                sellers_rating=sellers_rating,
                                timestamp=timestamp)
            price_entries.append(price_entry)

        return price_entries

    def make_statistics_entry(self, statistics_dict):
        average = statistics_dict["average"]
        minimum = statistics_dict["minimum"]
        maximum = statistics_dict["maximum"]
        timestamp = datetime.now()

        statistics_entry = Statistic(average=average,
                                     minimum=minimum,
                                     maximum=maximum,
                                     timestamp=timestamp)

        return statistics_entry

    def get_or_create_book_entry(self, info, profits, book_link, asin):
        book_entry = None
        existing_book_entry = self.session.query(Book).filter(Book.link == book_link).first()

        if existing_book_entry == None:
            isbn = info["isbn"]
            title = info["title"]
            sales_rank = info["sales_rank"]
            cover_type = info["cover_type"]

            profit_like_new = profits["profit_like_new"]
            profit_very_good = profits["profit_very_good"]
            profit_ratio = profits["profit_ratio"]
            profit_trade_in = -1

            newer_edition_available = False
            newer_edition_link = ""
            trade_in_available = False
            trade_in_price = -1

            block_until = datetime(1000, 1, 1)

            book_entry = Book(link=book_link,
                              asin=asin,
                              title=title,
                              isbn=isbn,
                              newer_edition_available=newer_edition_available,
                              newer_edition_link=newer_edition_link,
                              trade_in_available=trade_in_available,
                              trade_in_price=trade_in_price,
                              rank=sales_rank,
                              cover_type=cover_type,
                              profit_like_new=profit_like_new,
                              profit_very_good=profit_very_good,
                              profit_trade_in=profit_trade_in,
                              profit_ratio=profit_ratio,
                              block_until=block_until)

            return book_entry
        else:
            return existing_book_entry