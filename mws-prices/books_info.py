import bottlenose
import time
import json
import requests
from bs4 import BeautifulSoup


PRODUCT_API_URL = "http://207.148.10.85:80/product_api/v1.0/"


class BooksInfo:
    def __init__(self):
        self.amazon = bottlenose.Amazon("AKIAI3IHII5JDRKSVXOA",
                                        "qTwkNTFJxZQI5RsGJ3+VTdsu5BgWLTqE23ZMKcuq",
                                        "petsprostor09-20")
        self.accumulator = {}
        self.bundle_mode = False

    def get_accumulated_info(self):
        accumulator_result = self.accumulator.copy()
        self.accumulator.clear()

        return accumulator_result

    def is_bundle_mode(self):
        return self.bundle_mode

    def set_bundle_mode(self, bundle_mode):
        self.bundle_mode = bundle_mode

    def is_enough(self):
        if self.bundle_mode == False:
            return True

        if len(self.accumulator.keys()) >= 5:
            return True
        else:
            return False

    def process_asin(self, asin):
        print "For ASIN: " + asin
        response = self.make_request_to_server(asin)

        if "error" in response:
            print response
            return False

        self.accumulate_book_info(asin, response)
        return True

    def make_request_to_server(self, asin):
        response = ""
        response = requests.get(PRODUCT_API_URL + asin)

        return response.text

    def accumulate_book_info(self, asin, response_api):
        book_info = json.loads(response_api)

        self.accumulator[asin] = book_info

