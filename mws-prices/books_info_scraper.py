from models import *
from bs4 import BeautifulSoup
import bottlenose
import time
import random
from grab import Grab
from prices_scraper import PricesScraper
from datetime import date, timedelta, datetime

class BooksInfoScraper:
    def __init__(self):
        self.prices_scraper = PricesScraper()
        self.session = Session()
        metadata.create_all(bind=engine)
        self.g = Grab()

        proxies_entries = self.session.query(Proxy).all()
        self.proxies_list = []
        for proxy_entry in proxies_entries:
            self.proxies_list.append(proxy_entry.proxy)
        self.current_proxy = random.choice(self.proxies_list)

        self.amazon = bottlenose.Amazon("AKIAI4GSYFPSBF2FHMYA",
                                        "YirCLdUMCEWh/H4VaklmAvJ+m2NhOMoqRQq7pmEY",
                                        "petsprostore-20")
        self.info_operation = 0
        self.trade_in_newer_operation = 0
        self.prices_operation = 0

    def switch_proxy(self):
        self.current_proxy = random.choice(self.proxies_list)
        self.g.setup(proxy=self.current_proxy, proxy_type="http", connect_timeout=5, timeout=5)

    def run(self, start, end):
        self.obtain_links_for_books(start, end)
        print "Starting to scrape the prices."

        # self.obtain_book_info(asins[3])

        for book_link in self.most_selling_dict.keys():
            self.obtain_book_info(book_link)

    def obtain_links_for_books(self, start, end):
        latest_book_link = self.session.query(BookLink).order_by(BookLink.date)[0]
        latest_date = latest_book_link.date
        latest_date = latest_date - timedelta(days=1)

        self.most_selling_dict = {}
        book_link_entries = self.session.query(BookLink).\
                                filter(BookLink.date >= latest_date).\
                                order_by(BookLink.id)[start:end]

        for book_link_entry in book_link_entries:
            self.most_selling_dict[book_link_entry.link] = book_link_entry.category

    def obtain_book_info(self, book_link):
        asin = self.obtain_asin_from_link(book_link)
        asin = asin.replace("\n", "")
        response = ""

#==================================================================================================================
        t1 = datetime.now()
        while True:
            print "For ASIN: " + asin
            try:
                response = self.amazon.ItemLookup(ItemId=asin, ResponseGroup="SalesRank,ItemAttributes")
            except:
                time.sleep(1)
                continue
            break
        t2 = datetime.now()

        td = t2 - t1
        self.info_operation += td.total_seconds()
#==================================================================================================================

        self.save_book_to_db(response, book_link)

    def save_book_to_db(self, xml_book, book_link):
        soup = BeautifulSoup(xml_book, "xml")

        sales_ranks = soup.select("SalesRank")
        sales_rank = 1000000
        if len(sales_ranks) > 0:
            sales_rank = int(sales_ranks[0].get_text())
        if sales_rank > 300000:
            return False

        cover_types = soup.select("Binding")
        cover_type = ""
        if len(cover_types) > 0:
            cover_type = cover_types[0].get_text()
        if cover_type != "Hardcover" and cover_type != "Paperback":
            return False

        isbns = soup.select("ISBN")
        isbn = ""
        if len(isbns) > 0:
            isbn = isbns[0].get_text()

        titles = soup.select("Title")
        title = ""
        if len(titles) > 0:
            title = titles[0].get_text()

        newer_link_available = False
        trade_in_available = False

#==================================================================================================================
        # t1 = datetime.now()
        # newer_trade_in = self.check_link_for_newer_trade_in(book_link)
        # t2 = datetime.now()
        #
        # td = t2 - t1
        # self.trade_in_newer_operation += td.total_seconds()
#==================================================================================================================
        newer_trade_in = {}
        newer_trade_in["newer"] = ""
        try:
            if newer_trade_in["newer"] == "":
                newer_link_available = False
            else:
                newer_link_available = True
        except:
            pass

        newer_trade_in["trade_in"] = ""

        trade_in = newer_trade_in["trade_in"]
        trade_in_price = -1

        if trade_in != "":
            print "Trade-in available: " + trade_in
            trade_in_available = True
            trade_in = trade_in.replace("$", "")
            trade_in_price = float(trade_in)

        asin = self.obtain_asin_from_link(book_link)

#==================================================================================================================
        t1 = datetime.now()
        prices_entries = self.prices_scraper.obtain_prices_for_asin(asin)
        t2 = datetime.now()

        td = t2 - t1
        self.prices_operation += td.total_seconds()
#==================================================================================================================

        profit_like_new = self.calculate_profit_like_new(prices_entries)
        profit_very_good = self.calculate_profit_very_good(prices_entries)
        profit_trade_in = self.calculate_profit_trade_in(prices_entries, trade_in_price)
        profit_ratio = self.calculate_profit_ratio(profit_like_new, profit_very_good ,prices_entries)

        block_until = datetime(1000, 1, 1)

        book_entry = Book(link=book_link,
                          asin=asin,
                          title=title,
                          isbn=isbn,
                          newer_edition_available=newer_link_available,
                          newer_edition_link=newer_trade_in["newer"],
                          trade_in_available=trade_in_available,
                          trade_in_price=trade_in_price,
                          rank=sales_rank,
                          cover_type=cover_type,
                          profit_like_new=profit_like_new,
                          profit_very_good=profit_very_good,
                          profit_trade_in=profit_trade_in,
                          profit_ratio=profit_ratio,
                          block_until=block_until)

        book_entry.category = self.most_selling_dict[book_link]
        book_entry.prices = prices_entries

        self.session.add(book_entry)

        try:
            self.session.commit()
        except:
            print("The database already has this book.")
            self.session.rollback()
            return

        print "Successfully saved in the database."

    def check_link_for_newer_trade_in(self, book_link):
        newer_trade_in = {}
        newer_trade_in["newer"] = ""
        newer_trade_in["trade_in"] = ""

        link_newer = ""
        trade_in = ""
        self.switch_proxy()
        try:
            self.g.go(book_link)
        except:
            self.switch_proxy()
            try:
                self.g.go(book_link)
            except:
                return newer_trade_in

        link_nodes = self.g.doc.select("//div[@id='newer-version']//a[@class='a-size-base a-link-normal']")

        if len(link_nodes) > 0:
            link_node = link_nodes[0]
            link_newer = "https://www.amazon.com" + link_node.attr("href")

        trade_in_nodes = self.g.doc.select("//span[@id='tradeInButton_tradeInValue']")
        if len(trade_in_nodes) > 0:
            trade_in_node = trade_in_nodes[0]
            trade_in = trade_in_node.text()

        newer_trade_in["newer"] = link_newer
        newer_trade_in["trade_in"] = trade_in

        return newer_trade_in

    def obtain_asin_from_link(self, book_link):
        asin = ""
        splitted_link = book_link.split("dp/")

        if len(splitted_link) == 2:
            without_dp = splitted_link[1]
            asin = without_dp.split("/")[0]

        return asin

    def calculate_profit_like_new(self, prices_entries_list):
        new_minimal_price = 100000
        new_minimal_shipping = 100000

        like_new_minimal_price = 100000
        like_new_minimal_shipping = 100000

        for price_entry in prices_entries_list:
            if price_entry.condition == "New":
                if (price_entry.price + price_entry.shipping ) < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_entry.price
                    new_minimal_shipping = price_entry.shipping

            if price_entry.condition == "Like new":
                if (price_entry.price + price_entry.shipping ) < (like_new_minimal_price + like_new_minimal_shipping):
                    like_new_minimal_price = price_entry.price
                    like_new_minimal_shipping = price_entry.shipping

        if new_minimal_price == 100000 or new_minimal_shipping == 100000 or like_new_minimal_price == 100000 or like_new_minimal_shipping == 100000:
            return 0
        else:
            profit = (new_minimal_price + new_minimal_shipping) * 0.84 - \
                     (like_new_minimal_price + like_new_minimal_shipping) - 1.35
            return profit

    def calculate_profit_very_good(self, prices_entries_list):
        new_minimal_price = 100000
        new_minimal_shipping = 100000

        very_good_minimal_price = 100000
        very_good_minimal_shipping = 100000

        for price_entry in prices_entries_list:
            if price_entry.condition == "New":
                if (price_entry.price + price_entry.shipping ) < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_entry.price
                    new_minimal_shipping = price_entry.shipping

            if price_entry.condition == "Very good":
                if (price_entry.price + price_entry.shipping ) < (very_good_minimal_price + very_good_minimal_shipping):
                    very_good_minimal_price = price_entry.price
                    very_good_minimal_shipping = price_entry.shipping

        if new_minimal_price == 100000 or new_minimal_shipping == 100000 or very_good_minimal_price == 100000 or very_good_minimal_shipping == 100000:
            return 0
        else:
            profit = (new_minimal_price + new_minimal_shipping) * 0.84 - \
                     (very_good_minimal_price + very_good_minimal_shipping) - 1.35
            return profit

    def calculate_profit_ratio(self, like_new, very_good, prices_entries_list):
        minimal_profit = 0
        two_prices = [like_new, very_good]
        two_prices = [elem for elem in two_prices if elem > 0]
        if len(two_prices) > 0:
            minimal_profit = min(two_prices)

        new_minimal_price = 100000
        new_minimal_shipping = 100000

        for price_entry in prices_entries_list:
            if price_entry.condition == "New":
                if (price_entry.price + price_entry.shipping) < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_entry.price
                    new_minimal_shipping = price_entry.shipping

        if new_minimal_price == 100000 or new_minimal_shipping == 100000:
            return 0
        else:
            profit_ratio = minimal_profit / (new_minimal_price + new_minimal_shipping)
            return profit_ratio

    def calculate_profit_trade_in(self, prices_entries_list, trade_in_price):
        if trade_in_price == -1:
            return 0

        minimal_price = 100000
        minimal_shipping = 100000

        for price_entry in prices_entries_list:
            if (price_entry.price + price_entry.shipping) < (minimal_price + minimal_shipping):
                minimal_price = price_entry.price
                minimal_shipping = price_entry.shipping

        if minimal_price == 100000 or minimal_shipping == 100000:
            return 0
        else:
            trade_in_profit = trade_in_price - (minimal_price + minimal_shipping)
            return trade_in_profit

    def get_info_operation(self):
        return self.info_operation

    def get_trade_in_newer_operation(self):
        return self.trade_in_newer_operation

    def get_prices_operation(self):
        return self.prices_operation



