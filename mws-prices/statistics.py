class Statistics:
    def __init__(self):
        pass

    def calculate(self, prices_dict):
        self.prices_dict = prices_dict
        statistics_all = {}
        for asin in self.prices_dict.keys():
            statistics_all[asin] = self.calculate_for_asin(prices_dict[asin])

        return statistics_all

    def calculate_for_asin(self, price_list):
        statistics_dict = {}
        statistics_dict["average"] = self.calculate_average_price(price_list)
        statistics_dict["minimum"] = self.calculate_minimum_price(price_list)
        statistics_dict["maximum"] = self.calculate_maximum_price(price_list)

        return statistics_dict

    def calculate_minimum_price(self, price_list):
        minimum_price = 100000
        minimum_shipping = 100000

        for price_dict in price_list:
            if (price_dict["price"] + price_dict["shipping"]) < (minimum_price + minimum_shipping):
                minimum_price = price_dict["price"]
                minimum_shipping = price_dict["shipping"]

        return minimum_price + minimum_shipping

    def calculate_maximum_price(self, price_list):
        maximum_price = 0
        maximum_shipping = 0

        for price_dict in price_list:
            if (price_dict["price"] + price_dict["shipping"]) > (maximum_price + maximum_shipping):
                maximum_price = price_dict["price"]
                maximum_shipping = price_dict["shipping"]

        return maximum_price + maximum_shipping

    def calculate_average_price(self, price_list):
        price_accumulator = 0
        shipping_accumulator = 0

        for price_dict in price_list:
            price_accumulator += price_dict["price"]
            shipping_accumulator += price_dict["shipping"]

        return (price_accumulator + shipping_accumulator) / len(price_list)