from price_list import PriceList
from exceptions import Exception


class PriceChecker:
    def check_item(self, result, asin):
        # if len(item.keys()) > 1:
        #     print "The item contains more than one ASIN"
        #     raise Exception
        #
        # asin = item.keys()[0]
        # price_list = item[asin]

        print "================="
        print "ASIN: " + asin
        print result
        print "================="

        for price_dict in result:
            self.check_price_dict(price_dict, asin)

    def check_price_dict(self, price_dict, asin):
        try:
            sellers_rating = str(price_dict["sellers_rating"])
        except:
            print "Seller's rating is not a string"
            print "ASIN: " + asin
            raise Exception

        try:
            subcondition = str(price_dict["subcondition"])
        except:
            print "Subcondition is not a string"
            print "ASIN: " + asin
            raise Exception

        try:
            price = float(price_dict["price"])
        except:
            print "Price is not a float"
            print "ASIN: " + asin
            raise Exception

        try:
            shipping = float(price_dict["shipping"])
        except:
            print "Shipping is not a float"
            print "ASIN: " + asin
            raise Exception


def main():
    asins = ["0140102310", "0140436049", "0847831728", "1591931991"]
    pr = PriceList()
    pr.set_asins(asins)
    results = pr.get_prices()

    print results

    checker = PriceChecker()

    for item in results.keys():
        checker.check_item(results[item], item)

    print "The test is completed succesfully."

if __name__ == "__main__":
    main()

