from models import *
import time
from mws import mws
from bs4 import BeautifulSoup


MWS_ACCESS_KEY = 'AKIAJHCGHX5QYSBUNKRQ'
MWS_SECRET_KEY = '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'
ACCOUNT_ID = 'AWM2BZYAVU72X'
MARKETPLACE_ID = 'ATVPDKIKX0DER'

# MWS_ACCESS_KEY = 'AKIAJMQNDUPM5MMTQUUA'
# MWS_SECRET_KEY = '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'
# ACCOUNT_ID = 'AQEMUNHNLWVX4'
# MARKETPLACE_ID = 'ATVPDKIKX0DER'


class PricesScraper:
    def __init__(self):
        self.session = Session()
        self.products = mws.Products(MWS_ACCESS_KEY, MWS_SECRET_KEY, ACCOUNT_ID)

        metadata.create_all(bind=engine)

    def __del__(self):
        self.session.close()

    def obtain_prices_for_asin(self, asin):
        res_new = ""
        res_used = ""
        asin = asin.replace("\n", "")
        print "Prices for ASIN: " + asin

        while True:
            print "Trying new."
            try:
                res_new = self.products.get_lowest_offer_listings_for_asin(MARKETPLACE_ID, [asin], condition="New")
            except:
                time.sleep(1)
                continue
            break

        prices_entries_new = self.parse_prices(res_new)

        while True:
            print "Trying used."
            try:
                res_used = self.products.get_lowest_offer_listings_for_asin(MARKETPLACE_ID, [asin], condition="Used")
            except:
                time.sleep(1)
                continue
            break

        prices_entries_used = self.parse_prices(res_used)

        print "I obtained prices for a book."

        return prices_entries_new + prices_entries_used

    def parse_prices(self, prices_xml_string):
        prices_entries = []
        soup = BeautifulSoup(prices_xml_string, "xml")

        for listing in soup.find_all("LowestOfferListing"):
            subconditions = listing.select("ItemSubcondition")
            subcondition = ""
            if len(subconditions) > 0:
                if subconditions[0].get_text() == "New":
                    subcondition = "New"
                elif subconditions[0].get_text() == "Mint":
                    subcondition = "Like new"
                elif subconditions[0].get_text() == "VeryGood":
                    subcondition = "Very good"
                elif subconditions[0].get_text() == "Good":
                    subcondition = "Good"
                else:
                    continue

            sellers_ratings = listing.select("SellerPositiveFeedbackRating")
            sellers_rating = ""
            if len(sellers_ratings) > 0:
                sellers_rating = sellers_ratings[0].get_text()

            prices = listing.select("ListingPrice Amount")
            price = ""
            if len(prices) > 0:
                price = prices[0].get_text()
            price = price.replace(",", "")


            shippings = listing.select("Shipping Amount")
            shipping = ""
            if len(shippings) > 0:
                shipping = shippings[0].get_text()
            shipping = shipping.replace(",", "")

            price_number = -1
            try:
                price_number = float(price)
            except:
                pass

            shipping_number = -1
            try:
                shipping_number = float(shipping)
            except:
                pass

            price_entry = Price(price=price_number,
                                shipping=shipping_number,
                                condition=subcondition,
                                sellers_rating=sellers_rating)
            prices_entries.append(price_entry)

        return prices_entries

        # lowest_offer_listing_nodes = tree.xpath("//LowestOfferListing")
        # for node in lowest_offer_listing_nodes:
        #     print node.text