from books_info import BooksInfo
from exceptions import Exception


class BooksInfoChecker:
    def check_info(self, info_dict, asin):
        try:
            sales_rank = int(info_dict["sales_rank"])
        except:
            print "Sales rank is not int."
            print "ASIN: " + asin
            raise Exception

        try:
            isbn = str(info_dict["isbn"])
        except:
            print "ISBN is not a string."
            print "ASIN: " + asin
            raise Exception

        try:
            title = str(info_dict["title"])
        except:
            print "Title is not a string."
            print "ASIN: " + asin
            raise Exception

        try:
            cover_type = str(info_dict["cover_type"])
        except:
            print "Cover type is not a string."
            print "ASIN: " + asin
            raise Exception

    def check_profits(self, profits_dict, asin):
        try:
            profit_like_new = float(profits_dict["profit_like_new"])
        except:
            print "Profit for like new condition is not float."
            print "ASIN: " + asin
            raise Exception

        try:
            profit_very_good = float(profits_dict["profit_very_good"])
        except:
            print "Profit for very good condition is not float."
            print "ASIN: " + asin
            raise Exception

        try:
            profit_ratio = float(profits_dict["profit_ratio"])
        except:
            print "Profit ratio is not float."
            print "ASIN: " + asin
            raise Exception


def main():
    asins = ["0140102310", "0140436049", "0847831728", "1591931991"]
    books_info = BooksInfo()

    for asin in asins:
        books_info.process_asin(asin)

    results = books_info.get_accumulated_info()
    print results

    checker = BooksInfoChecker()

    for asin in results.keys():
        checker.check_info(results[asin], asin)

    # for item in results:
    #     checker.check_item(item)

    print "The test is completed succesfully."

if __name__ == "__main__":
    main()