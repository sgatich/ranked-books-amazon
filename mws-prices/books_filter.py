import requests
from models import *


PRODUCT_API_URL = "http://207.148.10.85:80/product_api/v1.0/"


class BooksFilter:
    def __init__(self):
        self.session = Session()

    def run(self):
        book_link_entries = self.read_entries_from_db()

        for book_link_entry in book_link_entries:
            asin = self.get_asin_from_link(book_link_entry.link)
            does_fit = self.check_asin(asin)

            if does_fit:
                self.save_entry_to_db(book_link_entry)

    def read_entries_from_db(self):
        latest_book_link = self.session.query(BookLink).order_by(BookLink.date)[0]
        latest_date = latest_book_link.date
        book_link_entries = self.session.query(BookLink).filter(BookLink.date == latest_date).order_by(BookLink.id)[:]

        return book_link_entries

    def get_asin_from_link(self, book_link):
        asin = ""
        splitted_link = book_link.split("dp/")

        if len(splitted_link) == 2:
            without_dp = splitted_link[1]
            asin = without_dp.split("/")[0]

        return asin

    def check_asin(self, asin):
        response = requests.get(PRODUCT_API_URL + asin)

        print response.text

        if "error" in response.text:
            print "The product doesn't fit."
            return False

        print "The product is OK."
        return True

    def save_entry_to_db(self, book_link_entry):
        link_filtered_entry = BookLinkFiltered()
        link_filtered_entry.link = book_link_entry.link
        link_filtered_entry.date = book_link_entry.date

        self.session.add(link_filtered_entry)
        self.session.commit()
