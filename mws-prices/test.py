from books_worker import BooksWorker

def main():
    scraper = BooksWorker(0, 500)
    scraper.start()

if __name__ == "__main__":
    main()