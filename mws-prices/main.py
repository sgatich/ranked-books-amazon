from mws import mws
from prices_scraper import PricesScraper
from books_worker import BooksWorker
from books_filter import BooksFilter
from links_worker import LinksWorker
from models import *

MWS_ACCESS_KEY = 'AKIAJHCGHX5QYSBUNKRQ'
MWS_SECRET_KEY = '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'
ACCOUNT_ID = 'AWM2BZYAVU72X'
MARKETPLACE_ID = 'ATVPDKIKX0DER'
ASINS = ['069289344X']
CONDITION = 'New'

def main():
    filter_books()

def get_books():
    workload = 20000 // 16

    first_scraper = BooksWorker(0, workload - 1)
    second_scraper = BooksWorker(workload, 2 * workload - 1)
    third_scraper = BooksWorker(2 * workload, 3 * workload - 1)
    fourth_scraper = BooksWorker(3 * workload, 4 * workload - 1)

    fifth_scraper = BooksWorker(4 * workload, 5 * workload - 1)
    sixth_scraper = BooksWorker(5 * workload, 6 * workload - 1)
    seventh_scraper = BooksWorker(6 * workload, 7 * workload - 1)
    eighth_scraper = BooksWorker(7 * workload, 8 * workload - 1)

    nineth_scraper = BooksWorker(8 * workload, 9 * workload - 1)
    tenth_scraper = BooksWorker(9 * workload, 10 * workload - 1)
    eleventh_scraper = BooksWorker(10 * workload, 11 * workload - 1)
    twelfth_scraper = BooksWorker(11 * workload, 12 * workload - 1)

    thirteenth_scraper = BooksWorker(12 * workload, 13 * workload - 1)
    fourteenth_scraper = BooksWorker(13 * workload, 14 * workload - 1)
    fifteenth_scraper = BooksWorker(14 * workload, 15 * workload - 1)
    sixteenth_scraper = BooksWorker(15 * workload, 9999)

    first_scraper.start()
    second_scraper.start()
    third_scraper.start()
    fourth_scraper.start()

    fifth_scraper.start()
    sixth_scraper.start()
    seventh_scraper.start()
    eighth_scraper.start()

    nineth_scraper.start()
    tenth_scraper.start()
    eleventh_scraper.start()
    twelfth_scraper.start()

    thirteenth_scraper.start()
    fourteenth_scraper.start()
    fifteenth_scraper.start()
    sixteenth_scraper.start()

def get_links():
    session = Session()

    number_categories = len(session.query(Category).all())

    workload = number_categories // 4

    first_worker = LinksWorker(0, workload - 1)
    second_worker = LinksWorker(workload, 2 * workload - 1)
    third_worker = LinksWorker(2 * workload, 3 * workload - 1)
    fourth_worker = LinksWorker(3 * workload, number_categories - 1)

    first_worker.start()
    second_worker.start()
    third_worker.start()
    fourth_worker.start()

def filter_books():
    books_filter = BooksFilter()
    books_filter.run()

if __name__ == "__main__":
    main()
