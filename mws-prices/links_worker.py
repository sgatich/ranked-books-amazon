import threading
from links_scraper import LinksScraper

class LinksWorker(threading.Thread):
    def __init__(self, start_category, end_category):
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self.start_category = start_category
        self.end_category = end_category

    def set_start_end(self, start_book, end_book):
        self.start_category = start_book
        self.end_category = end_book

    def run(self):
        self.scraper = LinksScraper()
        self.scraper.obtain_most_selling(self.start_category, self.end_category)

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()