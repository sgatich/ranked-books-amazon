import random
from models import *
from grab import Grab
from datetime import date, timedelta
from books_info import BooksInfo
from price_list import PriceList
from profits import Profits
from storage import Storage
from statistics import Statistics


class BooksController:
    def __init__(self):
        self.session = Session()
        metadata.create_all(bind=engine)
        self.g = Grab()

        proxies_entries = self.session.query(Proxy).all()
        self.proxies_list = []
        for proxy_entry in proxies_entries:
            self.proxies_list.append(proxy_entry.proxy)
        self.current_proxy = random.choice(self.proxies_list)

        self.books_info = BooksInfo()
        self.books_info.set_bundle_mode(True)

        self.price_list = PriceList()

        self.profits = Profits()
        self.storage = Storage()
        self.statistics = Statistics()

        self.current_asins = {}

    def run(self, start, end):
        self.obtain_links_for_books(start, end)
        print "Starting to scrape the prices."

        # self.obtain_book_info(asins[3])

        for asin in self.most_selling_dict.keys():
            self.obtain_book(asin)

    def obtain_book(self, asin):
        valid = self.books_info.process_asin(asin)

        if valid:
            self.add_current_asin(asin)

        if not self.books_info.is_enough():
            return

        info_dict = self.books_info.get_accumulated_info()
        self.price_list.set_asins(self.current_asins.keys())
        prices_dict = self.price_list.get_prices()
        profits_dict = self.profits.calculate(prices_dict)
        statistics_dict = self.statistics.calculate(prices_dict)

        self.storage.save_books(info_dict, prices_dict, profits_dict, self.current_asins, statistics_dict)
        self.current_asins.clear()

    def obtain_links_for_books(self, start, end):
        latest_book_link = self.session.query(BookLink).order_by(BookLink.date)[0]
        latest_date = latest_book_link.date
        latest_date = latest_date - timedelta(days=1)

        self.most_selling_dict = {}
        book_link_entries = self.session.query(BookLink).\
                                filter(BookLink.date >= latest_date).\
                                order_by(BookLink.id)[start:end]

        for book_link_entry in book_link_entries:
            most_selling_node = {}
            most_selling_node["category"] = book_link_entry.category
            most_selling_node["link"] = book_link_entry.link
            asin = self.obtain_asin_from_link(book_link_entry.link)
            self.most_selling_dict[asin] = most_selling_node

    def obtain_asin_from_link(self, book_link):
        asin = ""
        splitted_link = book_link.split("dp/")

        if len(splitted_link) == 2:
            without_dp = splitted_link[1]
            asin = without_dp.split("/")[0]

        return asin

    def add_current_asin(self, asin):
        self.current_asins[asin] = self.most_selling_dict[asin]