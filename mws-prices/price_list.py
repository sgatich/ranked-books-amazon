import time
import requests
import json
from mws import mws
from bs4 import BeautifulSoup


HOST = "http://45.76.16.56:80"

# MWS_ACCESS_KEY = 'AKIAJHCGHX5QYSBUNKRQ'
# MWS_SECRET_KEY = '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'
# ACCOUNT_ID = 'AWM2BZYAVU72X'
# MARKETPLACE_ID = 'ATVPDKIKX0DER'

MWS_ACCESS_KEY = 'AKIAJMQNDUPM5MMTQUUA'
MWS_SECRET_KEY = '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'
ACCOUNT_ID = 'AQEMUNHNLWVX4'
MARKETPLACE_ID = 'ATVPDKIKX0DER'


class PriceList:
    def __init__(self):
        self.mws_products = mws.Products(MWS_ACCESS_KEY, MWS_SECRET_KEY, ACCOUNT_ID)
        self.asins = None

    def __del__(self):
        pass

    def set_asins(self, asins):
        self.asins = asins

    def get_prices(self):
        http_request = HOST + "/mws_api/v1.0/" + ",".join(self.asins)

        response = requests.get(http_request)

        print response.text

        all_prices = {}
        all_prices = json.loads(response.text)

        return all_prices

        # prices = {}
        # if self.asins == None:
        #     return prices
        #
        # self.asins = self.delete_eol_from_asins(self.asins)
        #
        # print "Prices for ASINs: " + ", ".join(self.asins)
        #
        # self.make_request_to_mws()
        #
        # new_prices = self.parse_prices_for_asins(self.results_new)
        # used_prices = self.parse_prices_for_asins(self.results_used)
        # all_prices = self.merge_prices(new_prices, used_prices)
        #
        # self.asins = None
        #
        # return all_prices

    def make_request_to_mws(self):
        self.results_new = ""
        self.results_used = ""

        while True:
            print "Trying new."
            try:
                self.results_new = self.mws_products.get_lowest_offer_listings_for_asin(MARKETPLACE_ID,
                                                                               self.asins,
                                                                               condition="New")
            except:
                time.sleep(2)
                continue
            break

        while True:
            print "Trying used."
            try:
                self.results_used = self.mws_products.get_lowest_offer_listings_for_asin(MARKETPLACE_ID,
                                                                                self.asins,
                                                                                condition="Used")
            except:
                time.sleep(2)
                continue
            break

    def parse_prices_for_asins(self, asins_prices_results):
        asins_prices = {}
        soup = BeautifulSoup(asins_prices_results, "xml")
        asin_prices_results = soup.select("GetLowestOfferListingsForASINResult")

        for asin_prices_result in asin_prices_results:
            asin = self.get_asin_from_xml(asin_prices_result)
            asins_prices[asin] = self.parse_prices_for_asin(asin_prices_result)

        return asins_prices

    def parse_prices_for_asin(self, asin_prices_result):
        prices = []

        listing_nodes = asin_prices_result.select("LowestOfferListing")
        for listing_node in listing_nodes:
            price_dict = {}
            price_dict["subcondition"] = self.get_subcondition_from_xml(listing_node)
            price_dict["price"] = float(self.get_price_from_xml(listing_node))
            price_dict["shipping"] = float(self.get_shipping_from_xml(listing_node))
            price_dict["sellers_rating"] = self.get_sellers_rating_from_xml(listing_node)

            prices.append(price_dict)

        return prices

    def get_asin_from_xml(self, book_node):
        asins = book_node.select("Identifiers ASIN")
        asin = ""

        if len(asins) > 0:
            asin = asins[0].get_text()

        return asin

    def get_subcondition_from_xml(self, listing_node):
        subconditions = listing_node.select("ItemSubcondition")
        subcondition = ""

        if len(subconditions) > 0:
            if subconditions[0].get_text() == "New":
                subcondition = "New"
            elif subconditions[0].get_text() == "Mint":
                subcondition = "Like new"
            elif subconditions[0].get_text() == "VeryGood":
                subcondition = "Very good"
            elif subconditions[0].get_text() == "Good":
                subcondition = "Good"

        return subcondition

    def get_price_from_xml(self, listing_node):
        prices = listing_node.select("ListingPrice Amount")
        price = ""

        if len(prices) > 0:
            price = prices[0].get_text()
        price = price.replace(",", "")

        return price

    def get_shipping_from_xml(self, listing_node):
        shippings = listing_node.select("Shipping Amount")
        shipping = ""

        if len(shippings) > 0:
            shipping = shippings[0].get_text()
        shipping = shipping.replace(",", "")

        return shipping

    def get_sellers_rating_from_xml(self, listing_node):
        sellers_ratings = listing_node.select("SellerPositiveFeedbackRating")
        sellers_rating = ""

        if len(sellers_ratings) > 0:
            sellers_rating = sellers_ratings[0].get_text()

        return sellers_rating

    def delete_eol_from_asins(self, asins):
        processed_asins = []
        for asin in asins:
            asin = asin.replace("\n", "")
            processed_asins.append(asin)

        return processed_asins

    def merge_prices(self, new_prices, used_prices):
        all_prices = {}

        for asin in new_prices.keys():
            all_prices[asin] = new_prices[asin]

        for asin in used_prices.keys():
            if asin in all_prices:
                all_prices[asin] = all_prices[asin] + used_prices[asin]
            else:
                all_prices[asin] = used_prices[asin]

        return all_prices