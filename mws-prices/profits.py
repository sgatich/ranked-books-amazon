class Profits:
    def calculate(self, prices_dict):
        profits_all = {}
        for asin in prices_dict.keys():
            profits_for_asin = {}
            profits_for_asin["profit_like_new"] = self.calculate_profit_like_new(prices_dict[asin])
            profits_for_asin["profit_very_good"] = self.calculate_profit_very_good(prices_dict[asin])
            profits_for_asin["profit_ratio"] = self.calculate_profit_ratio(profits_for_asin["profit_like_new"],
                                                                           profits_for_asin["profit_very_good"],
                                                                           prices_dict[asin])

            profits_all[asin] = profits_for_asin

        return profits_all

    def calculate_profit_like_new(self, price_list):
        new_minimal_price = 100000
        new_minimal_shipping = 100000

        like_new_minimal_price = 100000
        like_new_minimal_shipping = 100000

        for price_dict in price_list:
            if price_dict["subcondition"] == "New":
                if (price_dict["price"] + price_dict["shipping"]) < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_dict["price"]
                    new_minimal_shipping = price_dict["shipping"]

            if price_dict["subcondition"] == "Like new":
                if (price_dict["price"] + price_dict["shipping"]) < (like_new_minimal_price + like_new_minimal_shipping):
                    like_new_minimal_price = price_dict["price"]
                    like_new_minimal_shipping = price_dict["shipping"]

        if new_minimal_price == 100000 or \
                        new_minimal_shipping == 100000 or \
                        like_new_minimal_price == 100000 or \
                        like_new_minimal_shipping == 100000:
            return 0
        else:
            profit = (new_minimal_price + new_minimal_shipping) * 0.84 - \
                     (like_new_minimal_price + like_new_minimal_shipping) - 1.35
            return profit

    def calculate_profit_very_good(self, price_list):
        new_minimal_price = 100000
        new_minimal_shipping = 100000

        very_good_minimal_price = 100000
        very_good_minimal_shipping = 100000

        for price_dict in price_list:
            if price_dict["subcondition"] == "New":
                if (price_dict["price"] + price_dict["shipping"]) < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_dict["price"]
                    new_minimal_shipping = price_dict["shipping"]

            if price_dict["subcondition"] == "Very good":
                if (price_dict["price"] + price_dict["shipping"]) < (very_good_minimal_price + very_good_minimal_shipping):
                    very_good_minimal_price = price_dict["price"]
                    very_good_minimal_shipping = price_dict["shipping"]

        if new_minimal_price == 100000 or \
                        new_minimal_shipping == 100000 or \
                        very_good_minimal_price == 100000 or \
                        very_good_minimal_shipping == 100000:
            return 0
        else:
            profit = (new_minimal_price + new_minimal_shipping) * 0.84 - \
                     (very_good_minimal_price + very_good_minimal_shipping) - 1.35
            return profit

    def calculate_profit_ratio(self, like_new, very_good, price_list):
        minimal_profit = 0
        two_prices = [like_new, very_good]
        two_prices = [elem for elem in two_prices if elem > 0]
        if len(two_prices) > 0:
            minimal_profit = min(two_prices)

        new_minimal_price = 100000
        new_minimal_shipping = 100000

        for price_dict in price_list:
            if price_dict["subcondition"] == "New":
                if (price_dict["price"] + price_dict["shipping"]) \
                        < (new_minimal_price + new_minimal_shipping):
                    new_minimal_price = price_dict["price"]
                    new_minimal_shipping = price_dict["shipping"]

        if new_minimal_price == 100000 or new_minimal_shipping == 100000:
            return 0
        else:
            profit_ratio = minimal_profit / (new_minimal_price + new_minimal_shipping)
            return profit_ratio

    def calculate_profit_trade_in(self):
        pass
