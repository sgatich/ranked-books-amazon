import thread
import random
from datetime import date
from models import *
from grab import Grab
from books_filter import BooksFilter


BASE_AMAZON_LINK = "https://www.amazon.com"


class LinksScraper:
    def __init__(self):
        self.session = Session()
        self.g = Grab()
        self.books_filter = BooksFilter()
        self.thread_id = thread.get_ident()

        proxies_entries = self.session.query(Proxy).all()
        self.proxies_list = []

        for proxy_entry in proxies_entries:
            self.proxies_list.append(proxy_entry.proxy)

        metadata.create_all(bind=engine)

    def switch_proxy(self):
        self.current_proxy = random.choice(self.proxies_list)
        print("Switching to proxy: " + self.current_proxy)
        self.g.setup(proxy=self.current_proxy, proxy_type="http", connect_timeout=10, timeout=10)

    def obtain_most_selling(self, start_category, end_category):
        print("Thread#" + str(self.thread_id) + " - Obtaining the most selling.")
        categories_list = self.session.query(Category).order_by(Category.id)[start_category:end_category]

        i = start_category
        for category in categories_list:
            self.switch_proxy()
            print("Thread#" + str(self.thread_id) + " - Category#" + str(i))
            self.obtain_most_selling_category(category.link, category)
            i += 1

        print("Thread#" + str(self.thread_id) + " - Links.run. Task finished. ")

    def obtain_most_selling_category(self, link_category, category_entry):
        link_category = link_category.split("ref")[0]
        link_category = link_category + "ref=zg_bs_pg_2?_encoding=UTF8&pg="

        most_selling_list = []
        for i in range(1, 6):
            current_link = link_category + str(i)
            print(current_link)
            try:
                self.g.go(current_link)
            except:
                self.switch_proxy()
                try:
                    self.g.go(current_link)
                except:
                    pass

            link_nodes = self.g.doc.select("//div[@id='zg_centerListWrapper']//a[@class='a-link-normal']")
            print(len(link_nodes))

            for link_node in link_nodes:
                link_node_divs = link_node.select(".//div")
                if len(link_node_divs) > 0:
                    print(link_node.attr("href"))
                    most_selling_list.append(link_node.attr("href"))

        for most_selling in most_selling_list:
            link = BASE_AMAZON_LINK + most_selling

            if self.books_filter.filter_book(link):
                self.save_book_db(link, category_entry)
            else:
                return

    def save_book_db(self, book_link, category_entry):
        book_link_entry = BookLink(link=book_link,
                                   date=date.today())
        book_link_entry.category = category_entry
        self.session.add(book_link_entry)

        self.session.commit()
        try:
            pass
        except:
            print("Thread#" + str(self.thread_id) + " - The database already has this link.")
            self.session.rollback()
            pass
