import smtplib
import random
from datetime import datetime
from models import *


EMAIL_LOGIN = "amazon.books.bot@gmail.com"
EMAIL_PASSWORD = "Mnbrty195"
TARGET_EMAIL = "sergeymezhericher@yahoo.com"


class Task:
    def __init__(self):
        pass

    def run(self):
        pass


class ApiCheckTask(Task):
    def __init__(self):
        Task.__init__(self)
        self.session = Session()
        self.mws_api_accounts = []
        self.product_api_accounts = []

        self.smtp_interface = smtplib.SMTP("smtp.gmail.com", 587)
        self.smtp_interface.starttls()
        self.smtp_interface.login("amazon.books.bot", "Mnbrty195")

    def run(self):
        self.load_api_accounts()
        self.check_api_accounts()

    def load_api_accounts(self):
        self.mws_api_accounts = self.session.query(MwsApiAccount)[:]
        self.product_api_accounts = self.session.query(ProductApiAccount)[:]

    def check_api_accounts(self):
        current_timestamp = datetime.now()

        for mws_api_account in self.mws_api_accounts:
            timedelta = mws_api_account.timestamp - current_timestamp
            if timedelta.days < 5:
                self.send_email_about_mws_api(mws_api_account)

        for product_api_account in self.product_api_accounts:
            timedelta = product_api_account.timestamp - current_timestamp
            if timedelta.days < 5:
                self.send_email_about_product_api()

    def send_email_about_mws_api(self, account_entry):
        message = "Subject: Account about to expire\nYour MWS API account will expire in 5 days. " \
                  "MWS_ACCESS_KEY: {}. MWS_SECRET_KEY: " \
                  "{}. ACCOUNT_ID: {}. MARKETPLACE_ID: {}".format(account_entry.access_key_id,
                                                                  account_entry.secret_key_id,
                                                                  account_entry.account_id,
                                                                  account_entry.marketplace_id)
        self.smtp_interface.send_message(EMAIL_LOGIN, TARGET_EMAIL, message)

        print "The message about the MWS API account has been sent."

    def send_email_about_product_api(self, account_entry):
        message = "Subject: Account about to expire\nYour Product API account will expire in 5 days. " \
                  "ASSOCIATE TAG: {}. ACCESS KEY: " \
                  "{}. SECRET KEY: {}".format(account_entry.associate_tag,
                                                                  account_entry.access_key_id,
                                                                  account_entry.secret_access_key)
        self.smtp_interface.send_message(EMAIL_LOGIN, TARGET_EMAIL, message)

        print "The message about the Product API account has been sent."


#works only for Linux (Ubuntu)!
class PasswordChangeTask(Task):
    def __init__(self):
        Task.__init__(self)
        self.randomizer = random.SystemRandom()

    def run(self):
        words = self.get_words()
        self.update_records()

    def get_words(self, words_number=4):
        all_words = []
        words = []
        with open("/usr/share/dict/british-english") as british_file:
            all_words = british_file.readlines()

        for i in range(0, words_number):
            words.append(self.randomizer.choice(all_words))

        return words


    def update_records(self):
        pass

