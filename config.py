import os
basedir = os.path.abspath(os.path.dirname(__file__))



class Config(object):
    WTF_CSRF_ENABLED = True
    SECRET_KEY = os.environ.get("SECRET_KEY") or "gf934Hd#0Fl"

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URI") or "postgresql://gsa:pass@localhost/amazon_books_dev"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
